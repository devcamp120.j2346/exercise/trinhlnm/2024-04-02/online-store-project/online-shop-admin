export const PRODUCT_FETCH_PENDING = 'Trạng thái đợi khi gọi API lấy danh sách product'
export const PRODUCT_FETCH_SUCCESS = 'Trạng thái thành công khi gọi API lấy danh sách product'
export const PRODUCT_FETCH_ERROR = 'Trạng thái lỗi khi gọi API lấy danh sách product'

export const INPUT_FILTER_NAME_CHANGE = "Input tên sản phẩm change";
export const INPUT_FILTER_MAX_PRICE_CHANGE = "Input giá max change";
export const INPUT_FILTER_MIN_PRICE_CHANGE = "Input giá min change";
export const INPUT_FILTER_PRODUCT_TYPE_CHANGE = "Input loại sản phẩm change";

export const SET_FILTER = "Set dữ liệu cho filter";
export const CLEAR_FILTER = "Xóa trắng dữ liệu filter";
export const SET_FILTER_VALUES = "Đặt lại giá trị các trường của filter theo giá trị đã lưu trong local storage";
export const CLEAR_FILTER_VALUES = "Xóa trắng giá trị các trường của filter";

export const PRODUCT_TYPE_FETCH_PENDING = 'Trạng thái đợi khi gọi API lấy danh sách product type'
export const PRODUCT_TYPE_FETCH_SUCCESS = 'Trạng thái thành công khi gọi API lấy danh sách product type'
export const PRODUCT_TYPE_FETCH_ERROR = 'Trạng thái lỗi khi gọi API lấy danh sách product type'

export const CREATE_PRODUCT_PENDING = "Trạng thái đợi khi gọi API tạo sản phẩm"
export const CREATE_PRODUCT_SUCCESS = "Trạng thái thành công khi gọi API tạo sản phẩm"
export const CREATE_PRODUCT_ERROR = "Trạng thái lỗi khi gọi API tạo sản phẩm"

export const PRODUCT_BY_ID_FETCH_PENDING = "Trạng thái đợi khi gọi API lấy thông tin product từ id";
export const PRODUCT_BY_ID_FETCH_SUCCESS = "Trạng thái thành công khi gọi API lấy thông tin product từ id";
export const PRODUCT_BY_ID_FETCH_ERROR = "Trạng thái lỗi khi gọi API lấy thông tin product từ id";

export const EDIT_PRODUCT_PENDING = "Trạng thái đợi khi gọi API sửa sản phẩm";
export const EDIT_PRODUCT_SUCCESS = "Trạng thái thành công khi gọi API sửa sản phẩm";
export const EDIT_PRODUCT_ERROR = "Trạng thái lỗi khi gọi API sửa sản phẩm";

export const DELETE_PRODUCT_PENDING = "Trạng thái đợi khi gọi API xóa sản phẩm";
export const DELETE_PRODUCT_SUCCESS = "Trạng thái thành công khi gọi API xóa sản phẩm";
export const DELETE_PRODUCT_ERROR = "Trạng thái lỗi khi gọi API xóa sản phẩm";

export const CLEAR_MESSAGE_API = "Xóa message của api đã gọi";

export const CLEAR_PRODUCT_BY_ID = "Xóa thông tin product by id";

export const PAGE_CHANGE_PAGINATION = "Sự kiện thay đổi trang";

export const PRODUCT_DEL_FETCH_PENDING = 'Trạng thái đợi khi gọi API lấy danh sách product đã xóa'
export const PRODUCT_DEL_FETCH_SUCCESS = 'Trạng thái thành công khi gọi API lấy danh sách product đã xóa'
export const PRODUCT_DEL_FETCH_ERROR = 'Trạng thái lỗi khi gọi API lấy danh sách product đã xóa'

export const PAGE_CHANGE_PAGINATION_DEL = "Sự kiện thay đổi trang product đã xóa";
