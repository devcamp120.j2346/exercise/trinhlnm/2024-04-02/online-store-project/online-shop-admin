export const ORDER_FETCH_PENDING = 'Trạng thái đợi khi gọi API lấy danh sách order';
export const ORDER_FETCH_SUCCESS = 'Trạng thái thành công khi gọi API lấy danh sách order';
export const ORDER_FETCH_ERROR = 'Trạng thái lỗi khi gọi API lấy danh sách order';
export const PAGE_CHANGE_PAGINATION_ORDER = "Sự kiện thay đổi trang của order";
 
export const INPUT_FILTER_CUSTOMER_CHANGE = "Input customer trong bộ lọc change";
export const INPUT_FILTER_PRODUCT_CHANGE = "Input product trong bộ lọc change";
export const INPUT_FILTER_SDTOGGLE_CHANGE = "Toggle shipped date trong bộ lọc change";
export const INPUT_FILTER_SDMIN_CHANGE = "Input shipped date ngày min trong bộ lọc change";
export const INPUT_FILTER_SDMAX_CHANGE = "Input shipped date ngày max trong bộ lọc change";
export const INPUT_FILTER_ODMIN_CHANGE = "Input order date ngày min trong bộ lọc change";
export const INPUT_FILTER_ODMAX_CHANGE = "Input order date ngày max trong bộ lọc change";
export const INPUT_FILTER_COSTMIN_CHANGE = "Input giá min đơn hàng trong bộ lọc change";
export const INPUT_FILTER_COSTMAX_CHANGE = "Input giá max đơn hàng trong bộ lọc change";

export const SET_FILTER_ORDER = "Set dữ liệu cho filter order";
export const CLEAR_FILTER_ORDER = "Xóa trắng dữ liệu filter order";
export const CLEAR_FILTER_ORDER_VALUES = "Xóa trắng giá trị các trường filter order";
export const SET_FILTER_ORDER_VALUES = "Đặt lại giá trị các trường của filter theo giá trị đã lưu trong local storage";

export const CREATE_ORDER_PENDING = "Trạng thái đợi khi gọi API tạo order";
export const CREATE_ORDER_SUCCESS = "Trạng thái thành công khi gọi API tạo order";
export const CREATE_ORDER_ERROR = "Trạng thái lỗi khi gọi API tạo order";

export const EDIT_ORDER_PENDING = "Trạng thái đợi khi gọi API sửa order";
export const EDIT_ORDER_SUCCESS = "Trạng thái thành công khi gọi API sửa order";
export const EDIT_ORDER_ERROR = "Trạng thái lỗi khi gọi API sửa order";

export const DELETE_ORDER_PENDING = "Trạng thái đợi khi gọi API xóa order";
export const DELETE_ORDER_SUCCESS = "Trạng thái thành công khi gọi API xóa order";
export const DELETE_ORDER_ERROR = "Trạng thái lỗi khi gọi API xóa order";

export const CLEAR_MESSAGE_API = "Xóa message của api create đã gọi";

export const CLEAR_MESSAGE_API_ORDER = "Xóa message của api order đã gọi";

export const ORDER_BY_ID_FETCH_PENDING = "Trạng thái đợi khi gọi API lấy thông tin order từ id";
export const ORDER_BY_ID_FETCH_SUCCESS = "Trạng thái thành công khi gọi API lấy thông tin order từ id";
export const ORDER_BY_ID_FETCH_ERROR = "Trạng thái lỗi khi gọi API lấy thông tin order từ id";

export const CLEAR_ORDER_BY_ID = "Xóa thông tin order by id";

export const ORDER_DEL_FETCH_PENDING = 'Trạng thái đợi khi gọi API lấy danh sách order đã xóa'
export const ORDER_DEL_FETCH_SUCCESS = 'Trạng thái thành công khi gọi API lấy danh sách order đã xóa'
export const ORDER_DEL_FETCH_ERROR = 'Trạng thái lỗi khi gọi API lấy danh sách order đã xóa'

export const PAGE_CHANGE_PAGINATION_ORDER_DEL = "Sự kiện thay đổi trang order đã xóa";