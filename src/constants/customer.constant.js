export const CUSTOMER_FETCH_PENDING = 'Trạng thái đợi khi gọi API lấy danh sách customer'
export const CUSTOMER_FETCH_SUCCESS = 'Trạng thái thành công khi gọi API lấy danh sách customer'
export const CUSTOMER_FETCH_ERROR = 'Trạng thái lỗi khi gọi API lấy danh sách customer'
export const PAGE_CHANGE_PAGINATION_CUSTOMER = "Sự kiện thay đổi trang của customer"

export const INPUT_FILTER_FULLNAME_CHANGE = "Input fullname khách hàng change";
export const INPUT_FILTER_CITY_CHANGE = "Input giá max change";
export const INPUT_FILTER_COUNTRY_CHANGE = "Input giá min change";

export const SET_FILTER_CUS = "Set dữ liệu cho filter customer";
export const CLEAR_FILTER_CUS = "Xóa trắng dữ liệu filter customer";
export const CLEAR_FILTER_CUS_VALUES = "Xóa giá trị các trường filter customer";
export const SET_FILTER_CUS_VALUES = "Đặt lại giá trị các trường của filter theo giá trị đã lưu trong local storage";

export const CLEAR_MESSAGE_API_CUSTOMER = "Xóa message của api customer đã gọi"

export const CREATE_CUSTOMER_PENDING = "Trạng thái đợi khi gọi API tạo customer"
export const CREATE_CUSTOMER_SUCCESS = "Trạng thái thành công khi gọi API tạo customer"
export const CREATE_CUSTOMER_ERROR = "Trạng thái lỗi khi gọi API tạo customer"

export const CUSTOMER_BY_ID_FETCH_PENDING = "Trạng thái đợi khi gọi API lấy thông tin customer từ id";
export const CUSTOMER_BY_ID_FETCH_SUCCESS = "Trạng thái thành công khi gọi API lấy thông tin customer từ id";
export const CUSTOMER_BY_ID_FETCH_ERROR = "Trạng thái lỗi khi gọi API lấy thông tin customer từ id";

export const EDIT_CUSTOMER_PENDING = "Trạng thái đợi khi gọi API sửa customer";
export const EDIT_CUSTOMER_SUCCESS = "Trạng thái thành công khi gọi API sửa customer";
export const EDIT_CUSTOMER_ERROR = "Trạng thái lỗi khi gọi API sửa customer";

export const DELETE_CUSTOMER_PENDING = "Trạng thái đợi khi gọi API xóa customer";
export const DELETE_CUSTOMER_SUCCESS = "Trạng thái thành công khi gọi API xóa customer";
export const DELETE_CUSTOMER_ERROR = "Trạng thái lỗi khi gọi API xóa customer";

export const CLEAR_CUSTOMER_BY_ID = "Xóa thông tin customer by id";

export const CUSTOMER_DEL_FETCH_PENDING = 'Trạng thái đợi khi gọi API lấy danh sách customer đã xóa'
export const CUSTOMER_DEL_FETCH_SUCCESS = 'Trạng thái thành công khi gọi API lấy danh sách customer đã xóa'
export const CUSTOMER_DEL_FETCH_ERROR = 'Trạng thái lỗi khi gọi API lấy danh sách customer đã xóa'

export const PAGE_CHANGE_PAGINATION_CUSTOMER_DEL = "Sự kiện thay đổi trang customer đã xóa";

export const COUNTRYCITY_FETCH_PENDING = 'Trạng thái đợi khi gọi API lấy danh sách quốc gia và thành phố'
export const COUNTRYCITY_FETCH_SUCCESS = 'Trạng thái thành công khi gọi API lấy danh sách quốc gia và thành phố'
export const COUNTRYCITY_FETCH_ERROR = 'Trạng thái lỗi khi gọi API lấy danh sách quốc gia và thành phố'