import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'

async function loginFetch(data, navigate) {

  try {
    const response = await fetch("http://localhost:8080/api/auth/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    const result = await response.json();
    console.log(result);
    if (result.message == "Login successfully") {
      sessionStorage.setItem("userId", result.userId);
      sessionStorage.setItem("accessToken", result.accessToken);
      sessionStorage.setItem("refreshToken", result.refreshToken);
      sessionStorage.setItem("startTime", new Date().getTime());

      navigate("/#/traffic", { replace: true });
    } else {
      alert(result.message);
    }
  } catch (error) {
    console.error(error);
    alert("Error");
  }
}

const Login = () => {
  const navigate = useNavigate();
  const [inputUsername, setInputUsername] = useState("");
  const [inputPass, setInputPass] = useState("");

  const onInputUsernameChange = (e) => {
    setInputUsername(e.target.value);
  }

  const onInputPassChange = (e) => {
    setInputPass(e.target.value);
  }

  const onGetStartedClicked = () => {
    var vCheck = true;
    if (inputUsername == "") {
      alert("User name required!");
      vCheck = false;
      return;
    }

    if (inputPass == "") {
      alert("Password required!");
      vCheck = false;
      return;
    }

    if (vCheck) {
      loginFetch({
        username: inputUsername,
        password: inputPass
      }, navigate);
    }
  }

  return (
    <div style={{ position: "relative" }} className="min-vh-100 d-flex flex-row align-items-center">
      <CContainer style={{ position: "relative" }}>
        <CRow className="justify-content-center">
          <CCol md={8}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>Login</h1>
                    <p className="text-body-secondary">Sign In to your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser} />
                      </CInputGroupText>
                      <CFormInput onChange={onInputUsernameChange} placeholder="Username" autoComplete="username" />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupText>
                        <CIcon icon={cilLockLocked} />
                      </CInputGroupText>
                      <CFormInput
                        onChange={onInputPassChange}
                        type="password"
                        placeholder="Password"
                        autoComplete="current-password"
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs={6}>
                        <CButton onClick={onGetStartedClicked} color="primary" className="px-4">
                          Login
                        </CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard className="text-white py-5" style={{ width: '44%', backgroundColor: "#038241" }}>
                <CCardBody className="text-center">
                  <div>
                    <h2>Sign up</h2>
                    <p>
                      Welcome to Online Shop Admin
                    </p>
                    <Link to="/register">
                      <CButton color="primary" className="mt-3" active tabIndex={-1}>
                        Register Now!
                      </CButton>
                    </Link>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>

      <div style={{ backgroundColor: "white", width: "7rem", height: "7rem", borderRadius: "50%", position: "absolute", bottom: "7rem", right: "0rem", zIndex: "-1" }}></div>
      <div style={{ backgroundColor: "#2acb78", width: "4.5rem", height: "4.5rem", borderRadius: "50%", position: "absolute", bottom: "0rem", right: "5rem", zIndex: "-1" }}></div>
      <div style={{ backgroundColor: "#2acb78", width: "3rem", height: "3rem", borderRadius: "50%", position: "absolute", bottom: "5rem", left: "1.5rem", zIndex: "-1" }}></div>
      <div style={{ backgroundColor: "#131312", width: "2rem", height: "2rem", borderRadius: "50%", position: "absolute", top: "1.5rem", right: "6rem", zIndex: "-1" }}></div>
      <div style={{ backgroundColor: "#2acb78", width: "8rem", height: "8rem", borderRadius: "50%", position: "absolute", top: "0rem", left: "11rem", zIndex: "-1" }}></div>
      <div style={{ backgroundColor: "white", width: "7rem", height: "7rem", borderRadius: "50%", position: "absolute", top: "7rem", left: "0rem", zIndex: "-1" }}></div>
    </div>
  )
}

export default Login
