import React, { useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
import { useNavigate } from 'react-router-dom';

async function postFetch(data, navigate) {

  try {
    const response = await fetch("http://localhost:8080/api/auth/signup?ad=true", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    const result = await response.json();
    console.log(result);
    if (result.message == "Create user successfully") {
      navigate("/login", { replace: true });
    } else {
      alert(result.message);
    }
  } catch (error) {
    console.error(error);
    alert("Error");
  }
}

const Register = () => {
  const navigate = useNavigate();
  const [inputUsername, setInputUsername] = useState("");
  const [inputPass, setInputPass] = useState("");

  const onInputUsernameChange = (e) => {
    setInputUsername(e.target.value);
  }

  const onInputPassChange = (e) => {
    setInputPass(e.target.value);
  }

  const onGetStartedClicked = () => {
    var vCheck = true;
    if (inputUsername == "") {
      alert("User name required!");
      vCheck = false;
      return;
    }

    if (inputPass == "") {
      alert("Password required!");
      vCheck = false;
      return;
    }

    if (vCheck) {
      postFetch({
        username: inputUsername,
        password: inputPass
      }, navigate);
    }
  }

  return (
    <div style={{ position: "relative" }} className="min-vh-100 d-flex flex-row align-items-center">
      <CContainer style={{ position: "relative" }}>
        <CRow className="justify-content-center">
          <CCol md={9} lg={7} xl={6}>
            <CCard className="mx-4">
              <CCardBody className="p-4">
                <CForm>
                  <h1>Register</h1>
                  <p className="text-body-secondary">Create your account</p>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput onChange={onInputUsernameChange} placeholder="Username" autoComplete="username" />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      type="password"
                      placeholder="Password"
                      autoComplete="new-password"
                      onChange={onInputPassChange}
                    />
                  </CInputGroup>
                  <div className="d-grid">
                    <CButton onClick={onGetStartedClicked} color="success">Create Account</CButton>
                  </div>
                </CForm>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>

      <div style={{ backgroundColor: "white", width: "7rem", height: "7rem", borderRadius: "50%", position: "absolute", bottom: "7rem", right: "0rem", zIndex: "-1" }}></div>
      <div style={{ backgroundColor: "#2acb78", width: "4.5rem", height: "4.5rem", borderRadius: "50%", position: "absolute", bottom: "0rem", right: "5rem", zIndex: "-1" }}></div>
      <div style={{ backgroundColor: "#2acb78", width: "3rem", height: "3rem", borderRadius: "50%", position: "absolute", bottom: "5rem", left: "1.5rem", zIndex: "-1" }}></div>
      <div style={{ backgroundColor: "#131312", width: "2rem", height: "2rem", borderRadius: "50%", position: "absolute", top: "1.5rem", right: "6rem", zIndex: "-1" }}></div>
      <div style={{ backgroundColor: "#2acb78", width: "8rem", height: "8rem", borderRadius: "50%", position: "absolute", top: "0rem", left: "11rem", zIndex: "-1" }}></div>
      <div style={{ backgroundColor: "white", width: "7rem", height: "7rem", borderRadius: "50%", position: "absolute", top: "7rem", left: "0rem", zIndex: "-1" }}></div>
    </div>
  )
}

export default Register
