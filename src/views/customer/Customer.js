/* eslint-disable react/jsx-key */
import { CAlert } from "@coreui/react";
import { Pagination, Tooltip } from "@mui/material";
import { useEffect, useState } from "react";
import { clearFilterCusAction, fetchCountryCity, fetchCustomer, fetchCustomerById, pageChangePaginationCustomer } from "src/actions/customer.action";
import { useStore2Dispatch, useStore2Selector } from "src/store";
import AddCustomerModal from "./AddCustomerModal";
import EditCustomerModal from "./EditCustomerModal";
import DeleteCustomerModal from "./DeleteCustomerModal";
import CIcon from "@coreui/icons-react";
import { cilLibraryAdd, cilFilterSquare } from '@coreui/icons';
import FilterCustomerModal from "./FilterCustomerModal";
import { clearFilterOrderAction, inputFilterCustomerChangeAction, setFilterOrderAction } from "src/actions/order.action";
import { useNavigate } from "react-router-dom";

/* eslint-disable react/react-in-jsx-scope */
const Customer = () => {
  const navigate = useNavigate();
  const dispatch = useStore2Dispatch();
  const { noPage, currentPage, limit, totalCustomers, customers, customerById, pending, error, filter } = useStore2Selector(reduxData => reduxData.customerReducer);

  const [visibleA, setVisibleA] = useState(false);
  const [visibleF, setVisibleF] = useState(false);
  const [visibleE, setVisibleE] = useState(false);
  const [visibleD, setVisibleD] = useState(false);

  useEffect(() => {
    dispatch(fetchCustomer(currentPage, limit, filter.fullName, filter.city, filter.country));
    dispatch(fetchCountryCity());
  }, [currentPage]);

  const onChangePagination = (event, value) => {
    dispatch(pageChangePaginationCustomer(value));
  }

  const onAModalClose = () => {
    setVisibleA(false);
  }

  const onFModalClose = () => {
    setVisibleF(false);
  }

  const onEModalClose = () => {
    setVisibleE(false);
  }

  const onDModalClose = () => {
    setVisibleD(false);
  }

  const onFilterTooltip = () => {
    dispatch(clearFilterCusAction());
    reloadPage();
  }

  const reloadPage = () => {
    window.location.reload();
  }

  const onAddCustomerBtnClicked = () => {
    setVisibleA(true);
  }

  const onFilterBtnClicked = () => {
    setVisibleF(true);
  }

  const onEditBtnClicked = (id) => {
    dispatch(fetchCustomerById(id));
    setVisibleE(!visibleE);
  }

  const onDeleteBtnClicked = (id) => {
    dispatch(fetchCustomerById(id));
    setVisibleD(!visibleD);
  }

  const onOrderClicked = (customer) => {
    dispatch(clearFilterOrderAction());
    dispatch(inputFilterCustomerChangeAction(customer));
    dispatch(setFilterOrderAction());
    navigate("/data/order");
  }

  console.log(customers);
  return (
    <>
      <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "1rem" }}>
      <div>
          <h3 style={{ display: "inline", color: "#1da35e", fontWeight: "700" }}>Customer</h3>
          <Tooltip title="Click to clear filter">
            <span onClick={onFilterTooltip} style={{ fontStyle: "italic", textDecoration: "underline", cursor: "pointer", fontSize: "0.9rem", marginLeft: "8px" }}>{Object.keys(filter).every((k) => !filter[k]) ? "" : "(Filter on)"}</span>
          </Tooltip>
        </div>

        <div style={{ display: "flex" }}>
          <Tooltip title="Filter list">
            <CIcon onClick={onFilterBtnClicked} style={{ width: "1.5rem", color: "#1da35e", marginRight: "0.5rem", cursor: "pointer" }} icon={cilFilterSquare} customClassName="nav-icon" />
          </Tooltip>
          <Tooltip title="Add new item">
            <CIcon onClick={onAddCustomerBtnClicked} style={{ width: "1.6rem", color: "#1da35e", cursor: "pointer" }} icon={cilLibraryAdd} customClassName="nav-icon" />
          </Tooltip>
        </div>
      </div>

      {
        customers.length != 0 ? <>
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th scope="col"></th>
                <th scope="col">Full name</th>
                <th scope="col">Phone</th>
                <th scope="col">Email</th>
                <th scope="col">Address</th>
                <th scope="col">Order</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              {
                pending ? <></> : customers.map((e, i) => {
                  return (
                    <tr>
                      <td style={{ fontWeight: "bold" }}>{i + (currentPage - 1) * limit + 1}</td>
                      <td>{e.fullName}</td>
                      <td>{e.phone}</td>
                      <td>{e.email}</td>
                      <td>{e.address}</td>
                      <td onClick={() => onOrderClicked(e)} style={{cursor: "pointer", textDecoration: "underline"}}>{e.orders.length}</td>
                      <td>
                        <button onClick={() => onEditBtnClicked(e._id)} style={{ color: "white", marginRight: "10px" }} className='btn btn-info'>Edit</button>
                        <button onClick={() => onDeleteBtnClicked(e._id)} style={{ color: "white" }} className='btn btn-danger'>Delete</button>
                      </td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>

          <div style={{ display: "flex", justifyContent: "space-between", margin: "1rem 0rem" }}>
            <div>Total: {totalCustomers} customers</div>
            <Pagination count={noPage} shape="rounded"
              onChange={onChangePagination}
              page={currentPage}
            />
          </div>
        </> : <div>No customers</div>
      }

      {
        error ? <CAlert color="danger" variant="solid" style={{ textAlign: "center", fontSize: "larger", fontWeight: "600" }}>
          Đã có lỗi!
        </CAlert> : <></>
      }

      <AddCustomerModal visible={visibleA} onClose={onAModalClose} reloadPage={reloadPage} />

      <EditCustomerModal visible={visibleE} onClose={onEModalClose} info={customerById} />

      <DeleteCustomerModal visible={visibleD} onClose={onDModalClose} info={customerById} />

      <FilterCustomerModal visible={visibleF} onClose={onFModalClose} reloadPage={reloadPage} />
    </>
  );
}
export default Customer;
