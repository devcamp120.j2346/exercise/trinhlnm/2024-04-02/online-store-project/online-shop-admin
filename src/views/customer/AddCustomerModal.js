/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import CIcon from "@coreui/icons-react";
import { cilBurn } from '@coreui/icons'
import { CAlert, CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { useEffect, useState } from "react";
import { useStore2Dispatch, useStore2Selector } from "src/store";
import { clearMessageApiCustomer, createCustomer, fetchCountryCity } from "src/actions/customer.action";

export const emailValidate = (email) => {
    const regexp = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/;
    return regexp.test(email);
}

export const phoneValidate = (phone) => {
    const regexp = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
    return regexp.test(phone);
}

const AddCustomerModal = ({ visible, onClose, reloadPage }) => {
    const dispatch = useStore2Dispatch();
    const { messageApi, countrycity } = useStore2Selector(reduxData => reduxData.customerReducer);

    const [addData, setAddData] = useState({
        fullName: "",
        phone: "",
        email: "",
        address: "",
        city: "",
        country: "",
    });
    const [selectedCountry, setSelectedCountry] = useState("");
    const [selectedCity, setSelectedCity] = useState("");
    const [errors, setErrors] = useState([]);

    const availableState = countrycity.find((c) => c.country === selectedCountry);

    const onCleanClose = () => {
        setAddData({
            fullName: "",
            phone: "",
            email: "",
            address: "",
            city: "",
            country: "",
        });
        setSelectedCountry("");
        setSelectedCity("");
        setErrors([]);
        dispatch(clearMessageApiCustomer());
        onClose();
    }

    const onAddBtnClicked = () => {
        dispatch(clearMessageApiCustomer());
        setErrors([]);

        var vErrors = [];

        if (addData.fullName == "" || addData.phone == "" || addData.email == "" || addData.address == "" || addData.city == "" || addData.country == "") {
            vErrors.push("Bạn chưa điền đủ thông tin!");
        }

        if (!phoneValidate(addData.phone)) {
            vErrors.push("Số điện thoại chưa đúng định dạng!");
        }

        if (!emailValidate(addData.email)) {
            vErrors.push("Email chưa đúng định dạng!");
        }

        setErrors(vErrors);

        if (vErrors.length == 0) {
            dispatch(createCustomer(addData));
        }
    }

    useEffect(() => {
        dispatch(fetchCountryCity());
        if (messageApi == "Tạo Customer thành công") {
            reloadPage();
        }
    }, [messageApi]);

    console.log(messageApi);
    return (
        <CModal
            visible={visible}
            onClose={onCleanClose}
            aria-labelledby="ScrollingLongContentExampleLabel"
        >
            <CModalHeader>
                <CModalTitle id="ScrollingLongContentExampleLabel">Add customer</CModalTitle>
            </CModalHeader>
            <CModalBody>
                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Full Name</label>
                    </div>
                    <div className="col-sm-9">
                        <input
                            type="text"
                            placeholder="Customer full name"
                            className="form-control"
                            onChange={(event) => {
                                setAddData({ ...addData, fullName: event.target.value });
                            }}
                        />
                    </div>
                </div>

                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Phone</label>
                    </div>
                    <div className="col-sm-9">
                        <input
                            type="text"
                            placeholder="Customer phone"
                            className="form-control"
                            onChange={(event) => {
                                setAddData({ ...addData, phone: event.target.value });
                            }}
                        />
                    </div>
                </div>

                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Email</label>
                    </div>
                    <div className="col-sm-9">
                        <input
                            type="text"
                            placeholder="Customer email"
                            className="form-control"
                            onChange={(event) => {
                                setAddData({ ...addData, email: event.target.value });
                            }}
                        />
                    </div>
                </div>

                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Address</label>
                    </div>
                    <div className="col-sm-9 d-flex align-items-center">
                        <input
                            type="text"
                            placeholder="Customer address"
                            className="form-control"
                            onChange={(event) => {
                                setAddData({ ...addData, address: event.target.value });
                            }}
                        />
                    </div>
                </div>

                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Country</label>
                    </div>
                    <div className="col-sm-9">
                        <select
                            type="text"
                            placeholder="Customer country"
                            className="form-control"
                            value={selectedCountry}
                            onChange={(event) => {
                                setSelectedCountry(event.target.value);
                                setAddData({ ...addData, country: event.target.value });
                            }}
                        >
                            <option>Select country</option>
                            {
                                countrycity ? countrycity.map((value, key) => {
                                    return (
                                        <option value={value.country} key={key}>
                                            {value.country}
                                        </option>
                                    );
                                }) : <></>
                            }
                        </select>
                    </div>
                </div>

                <div className="row form-group">
                    <div className="col-sm-3">
                        <label>City</label>
                    </div>
                    <div className="col-sm-9">
                        <select
                            type="text"
                            placeholder="Customer city"
                            className="form-control"
                            onChange={(event) => {
                                setSelectedCity(event.target.value);
                                setAddData({ ...addData, city: event.target.value });
                            }}
                            value={selectedCity}
                        >
                            <option>Select city</option>
                            {availableState?.cities.map((e, key) => {
                                return (
                                    <option value={e} key={key}>
                                        {e}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                </div>
            </CModalBody>
            <CModalFooter>
                {
                    errors.length == 0 ? <></> : <CAlert color="danger" className="d-flex w-100">
                        <CIcon icon={cilBurn} className="flex-shrink-0 me-2" width={24} height={24} />
                        <div>
                            <div style={{ fontWeight: "600" }}>Không thể thêm customer</div>
                            {
                                errors.map((e) => {
                                    return (
                                        <div>{e}</div>
                                    );
                                })
                            }
                        </div>
                    </CAlert>
                }

                {
                    (messageApi == "Tạo Customer thành công") ? <>
                        <CAlert color="success" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Tạo customer thành công!
                        </CAlert>
                    </> : <></>
                }

                {
                    (messageApi == "Unauthorized!") ? <>
                        <CAlert color="danger" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Tài khoản chưa được cấp quyền thực thi!
                        </CAlert>
                    </> : <></>
                }

                {
                    (messageApi != "" && messageApi != "Tạo Customer thành công" && messageApi != "Unauthorized!") ? <>
                        <CAlert color="danger" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Đã có lỗi!
                        </CAlert>
                    </> : <></>
                }

                <div>
                    <CButton color="secondary" onClick={onCleanClose}>
                        Close
                    </CButton>
                    <CButton onClick={onAddBtnClicked} color="success" style={{ color: "white", marginLeft: "8px" }}>Add customer</CButton>
                </div>
            </CModalFooter>
        </CModal>
    );
}

export default AddCustomerModal;