/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import CIcon from "@coreui/icons-react";
import { cilBurn } from '@coreui/icons'
import { CAlert, CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { useEffect, useState } from "react";
import { useStore2Dispatch, useStore2Selector } from "src/store";
import { clearFilterCusValues, fetchCountryCity, inputFilterCityChangeAction, inputFilterCountryChangeAction, inputFilterFullnameChangeAction, setFilterCusAction, setFilterCusValues } from "src/actions/customer.action";

const FilterCustomerModal = ({ visible, onClose, reloadPage }) => {
    const dispatch = useStore2Dispatch();
    const { countrycity, fullName, city, country, pendingCC } = useStore2Selector(reduxData => reduxData.customerReducer);

    const [errors, setErrors] = useState([]);
    const [selectedCountry, setSelectedCountry] = useState(country);
    const [selectedCity, setSelectedCity] = useState(city);

    const availableState = countrycity.find((c) => c.country === selectedCountry);

    const onCleanClose = () => {
        dispatch(clearFilterCusValues());
        setSelectedCountry("");
        setSelectedCity("");
        setErrors([]);
        onClose();
    }

    const onFilterBtnClicked = () => {
        setErrors([]);

        var vErrors = [];

        setErrors(vErrors);

        if (vErrors.length == 0) {
            dispatch(setFilterCusAction());
            reloadPage();
        }
    }

    const onClearFilterBtnClicked = () => {
        dispatch(inputFilterFullnameChangeAction(""));
        setSelectedCountry("");
        setSelectedCity("");
    }

    useEffect(() => {
        dispatch(setFilterCusValues());
        setSelectedCountry(country);
        setSelectedCity(city);
    }, [])

    return (
        <CModal
            visible={visible}
            onClose={onCleanClose}
            aria-labelledby="ScrollingLongContentExampleLabel"
        >
            <CModalHeader>
                <CModalTitle id="ScrollingLongContentExampleLabel">Filter customer</CModalTitle>
            </CModalHeader>
            <CModalBody style={{ paddingLeft: "3rem", paddingRight: "3rem" }}>
                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Full name</label>
                    </div>
                    <div className="col-sm-9">
                        <input
                            type="text"
                            placeholder="Customer full name"
                            className="form-control"
                            onChange={(event) => {
                                dispatch(inputFilterFullnameChangeAction(event.target.value));
                            }}
                            value={fullName}
                        />
                    </div>
                </div>

                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Country</label>
                    </div>
                    <div className="col-sm-9">
                        <select
                            type="text"
                            placeholder="Customer country"
                            className="form-control"
                            onChange={(event) => {
                                setSelectedCountry(event.target.value);
                                dispatch(inputFilterCountryChangeAction(event.target.value));
                            }}
                            value={selectedCountry}
                        >
                            <option>Select country</option>
                            {
                                countrycity ? countrycity.map((value, key) => {
                                    return (
                                        <option value={value.country} key={key}>
                                            {value.country}
                                        </option>
                                    );
                                }) : <></>
                            }
                        </select>
                    </div>
                </div>

                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>City</label>
                    </div>
                    <div className="col-sm-9">
                        <select
                            type="text"
                            placeholder="Customer city"
                            className="form-control"
                            onChange={(event) => {
                                setSelectedCity(event.target.value);
                                dispatch(inputFilterCityChangeAction(event.target.value));
                            }}
                            value={selectedCity}
                        >
                            <option>Select city</option>
                            {availableState?.cities.map((e, key) => {
                                return (
                                    <option value={e} key={key}>
                                        {e}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                </div>
            </CModalBody>
            <CModalFooter>
                {
                    errors.length == 0 ? <></> : <CAlert color="danger" className="d-flex w-100">
                        <CIcon icon={cilBurn} className="flex-shrink-0 me-2" width={24} height={24} />
                        <div>
                            <div style={{ fontWeight: "600" }}>Không thể lọc customer</div>
                            {
                                errors.map((e) => {
                                    return (
                                        <div>{e}</div>
                                    );
                                })
                            }
                        </div>
                    </CAlert>
                }

                <div>
                    <CButton color="secondary" onClick={onClearFilterBtnClicked}>
                        Clear
                    </CButton>
                    <CButton onClick={onFilterBtnClicked} color="success" style={{ color: "white", marginLeft: "8px" }}>Filter</CButton>
                </div>
            </CModalFooter>
        </CModal>
    );
}

export default FilterCustomerModal;