import { CCard, CCardBody, CCardHeader, CCol, CRow } from "@coreui/react";
import { CChartBar, CChartPolarArea } from "@coreui/react-chartjs";

/* eslint-disable react/react-in-jsx-scope */
const RatioCustomer = () => {
  return (
    <CRow>
      <CCol xs={6}>
        <CCard className="mb-4">
          <CCardHeader>Khách hàng đặt hàng nhiều nhất</CCardHeader>
          <CCardBody>
            <CChartPolarArea
              data={{
                labels: ['Name 1', 'Name 2', 'Name 3', 'Name 4', 'Khác'],
                datasets: [
                  {
                    data: [11, 16, 7, 3, 14],
                    backgroundColor: ['#FF6384', '#4BC0C0', '#FFCE56', '#E7E9ED', '#36A2EB'],
                  },
                ],
              }}
            />
          </CCardBody>
        </CCard>
      </CCol>

      <CCol xs={6}>
        <CCard className="mb-4">
          <CCardHeader>Thống kê theo thời gian</CCardHeader>
          <CCardBody>
            <CChartBar
              data={{
                labels: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7'],
                datasets: [
                  {
                    label: 'Name 2',
                    backgroundColor: '#4BC0C0',
                    data: [40, 20, 12, 39, 10, 40, 39, 80, 40],
                  },
                ],
              }}
              labels="months"
            />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
}
export default RatioCustomer;
