/* eslint-disable react/jsx-key */
import { CAlert } from "@coreui/react";
import { Pagination } from "@mui/material";
import { useEffect, useState } from "react";
import { editCustomer, fetchCustomerById, fetchCustomerDel, pageChangePaginationCustomerDel } from "src/actions/customer.action";
import { useStore2Dispatch, useStore2Selector } from "src/store";
import DelCustomerModal from "./DelCustomerModal";

/* eslint-disable react/react-in-jsx-scope */
const CustomerDel = () => {
  const dispatch = useStore2Dispatch();
  const { messageApi, noPage, currentPageDel, limit, totalCustomerDel, customerDels, customerById, pending, error } = useStore2Selector(reduxData => reduxData.customerReducer);

  const [visibleD, setVisibleD] = useState(false);

  useEffect(() => {
    dispatch(fetchCustomerDel(currentPageDel, limit));
  }, [currentPageDel]);

  useEffect(() => {
    if (messageApi == "Update thông tin Customer thành công") {
      window.location.reload();
    }
}, [messageApi]);

  const onChangePagination = (event, value) => {
    dispatch(pageChangePaginationCustomerDel(value));
  }

  const onRestoreBtnClicked = (id) => {
    dispatch(editCustomer({ isDel: false }, id));
  }

  const onDModalClose = () => {
    setVisibleD(false);
  }

  const onDeleteBtnClicked = (id) => {
    dispatch(fetchCustomerById(id));
    setVisibleD(!visibleD);
  }

  console.log(customerDels);
  return (
    <>
      <div style={{ display: "flex", justifyContent: "start", marginBottom: "1rem" }}>
        <h3 style={{ color: "#1da35e", fontWeight: "600" }}>Deleted Customer</h3>
      </div>

      {
        customerDels.length != 0 ? <>
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th scope="col"></th>
                <th scope="col">Full name</th>
                <th scope="col">Phone</th>
                <th scope="col">Email</th>
                <th scope="col">Address</th>
                <th scope="col">Order</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              {
                pending ? <></> : customerDels.map((e, i) => {
                  return (
                    <tr>
                      <td style={{ fontWeight: "bold" }}>{i + (currentPageDel - 1) * limit + 1}</td>
                      <td>{e.fullName}</td>
                      <td>{e.phone}</td>
                      <td>{e.email}</td>
                      <td>{e.address}</td>
                      <td>{e.orders.length}</td>
                      <td>
                        <button onClick={() => onRestoreBtnClicked(e._id)} style={{ color: "white", marginRight: "10px" }} className='btn btn-info'>Restore</button>
                        <button onClick={() => onDeleteBtnClicked(e._id)} style={{ color: "white" }} className='btn btn-danger'>Delete</button>
                      </td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>

          <div style={{ display: "flex", justifyContent: "space-between", margin: "1rem 0rem" }}>
            <div>Total: {totalCustomerDel} customers</div>
            <Pagination count={noPage} shape="rounded"
              onChange={onChangePagination}
              page={currentPageDel}
            />
          </div>
        </> : <div>No deleted customers</div>
      }

      {
        error ? <CAlert color="danger" variant="solid" style={{ textAlign: "center", fontSize: "larger", fontWeight: "600" }}>
          Đã có lỗi!
        </CAlert> : <></>
      }

      <DelCustomerModal visible={visibleD} onClose={onDModalClose} info={customerById} />
    </>
  );
}

export default CustomerDel;
