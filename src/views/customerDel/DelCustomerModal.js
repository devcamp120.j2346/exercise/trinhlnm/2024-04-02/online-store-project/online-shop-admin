/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { CAlert, CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { useEffect, useState } from "react";
import { clearCustomerByID, clearMessageApiCustomer, deleteCustomer, fetchCustomerDel } from "src/actions/customer.action";
import { useStore2Dispatch, useStore2Selector } from "src/store";

const DelCustomerModal = ({ visible, onClose, info }) => {
    const dispatch = useStore2Dispatch();
    const { messageApi, currentPageDel, limit } = useStore2Selector(reduxData => reduxData.customerReducer);

    const [customerData, setCustomerData] = useState({
        fullName: "",
        phone: "",
        email: "",
        address: "",
        city: "",
        country: "",
        orders: 0,
    });

    const onCleanClose = () => {
        setCustomerData({
            fullName: "",
            phone: "",
            email: "",
            address: "",
            city: "",
            country: "",
            orders: 0,
        });
        dispatch(clearMessageApiCustomer());
        dispatch(clearCustomerByID());
        onClose();
    }

    const onDeleteBtnClicked = () => {
        dispatch(deleteCustomer(info._id));
    }

    useEffect(() => {
        setCustomerData({
            fullName: info.fullName,
            phone: info.phone,
            email: info.email,
            address: info.address,
            city: info.city,
            country: info.country,
            orders: info.orders ? Object.keys(info.orders).length : 0
        });
    }, [info]);

    useEffect(() => {
        if (messageApi == "Xóa thông tin Customer thành công" || messageApi == "Không tìm thấy thông tin Customer") {
            dispatch(fetchCustomerDel(currentPageDel, limit));
        }
    }, [messageApi]);

    console.log(messageApi);
    return (
        <CModal
            visible={visible}
            onClose={onCleanClose}
            aria-labelledby="ScrollingLongContentExampleLabel"
        >
            <CModalHeader>
                <CModalTitle id="ScrollingLongContentExampleLabel">Delete customer</CModalTitle>
            </CModalHeader>
            <CModalBody style={{ textAlign: "center" }}>
                <div className="text-danger mb-3" style={{ fontWeight: "600", fontSize: "large" }}>
                    Khách hàng này hiện có {customerData.orders} đơn hàng.<br />
                    Nếu xóa khách hàng các đơn hàng liên quan sẽ bị xóa.<br />
                    Bạn có chắc muốn xóa customer này?
                </div>

                <div>Full name: {customerData.fullName}</div>
                <div>Phone: {customerData.phone}</div>
                <div>Email: {customerData.email}</div>
                <div>Address: {customerData.address}</div>
                <div>City: {customerData.city}</div>
                <div>Country: {customerData.country}</div>
            </CModalBody>
            <CModalFooter>
                {
                    (messageApi == "Xóa thông tin Customer thành công") ? <>
                        <CAlert color="success" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Xóa customer thành công!
                        </CAlert>
                    </> : <></>
                }

                {
                    (messageApi != "" && messageApi != "Xóa thông tin Customer thành công") ? <>
                        <CAlert color="danger" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Đã có lỗi!
                        </CAlert>
                    </> : <></>
                }

                <div>
                    <CButton color="secondary" onClick={onCleanClose}>
                        Close
                    </CButton>
                    <CButton onClick={onDeleteBtnClicked} color="success" style={{ color: "white", marginLeft: "8px" }}>Delete customer</CButton>
                </div>
            </CModalFooter>
        </CModal>
    );
}

export default DelCustomerModal;