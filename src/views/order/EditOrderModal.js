/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import CIcon from "@coreui/icons-react";
import { cilBurn } from '@coreui/icons'
import { CAlert, CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { useEffect, useState } from "react";
import { useStore2Dispatch, useStore2Selector } from "src/store";
import { convertDay } from "./Order";
import { clearMessageApiOrder, editOrder, fetchOrder } from "src/actions/order.action";

export const getMinShippesDate = (dayDDMMYYYY) => {
    var dateTokens = dayDDMMYYYY.split("/");
    var date = new Date(dateTokens[2], dateTokens[1] - 1, dateTokens[0]);
    return new Date(date.getTime() + 90000000).toJSON().slice(0, 10);
}

const EditOrderModal = ({ visible, onClose, info }) => {
    console.log(info);
    const dispatch = useStore2Dispatch();
    const { messageApi, currentPage, limit, filter } = useStore2Selector(reduxData => reduxData.orderReducer);

    const [addData, setAddData] = useState({
        fullName: "",
        phone: "",
        email: "",
        address: "",
        city: "",
        country: "",

        products: [],
        cost: 0,
        note: "",
        orderDate: "",

        shippedDate: "",
    });

    const [errors, setErrors] = useState([]);

    const onCleanClose = () => {
        setAddData({
            fullName: "",
            phone: "",
            email: "",
            address: "",
            city: "",
            country: "",

            products: [],
            cost: 0,
            note: "",
            orderDate: "",
            shippedDate: "",
        });
        setErrors([]);
        dispatch(clearMessageApiOrder());
        onClose();
    }

    const onEditBtnClicked = () => {
        dispatch(clearMessageApiOrder());
        setErrors([]);
        var vErrors = [];

        if (!addData.shippedDate) {
            vErrors.push("Bạn chưa chọn ngày giao hàng!");
        }

        setErrors(vErrors);

        if (vErrors.length == 0) {
            dispatch(editOrder({ shippedDate: addData.shippedDate }, info._id));
        }
    }

    useEffect(() => {
        if (info) {
            setAddData({
                fullName: info.customer.fullName,
                phone: info.customer.phone,
                email: info.customer.email,
                address: info.customer.address,
                city: info.customer.city,
                country: info.customer.country,

                products: info.orderDetails,
                cost: info.cost,
                note: info.note,
                orderDate: convertDay(info.orderDate),
                shippedDate: info.shippedDate ? convertDay(info.shippedDate) : "",
            });
        }
    }, [info]);

    useEffect(() => {
        if (messageApi == "Update thông tin order thành công") {
            dispatch(fetchOrder(currentPage, limit, filter));
        }
    }, [messageApi]);

    console.log(messageApi)
    return (
        <CModal
            visible={visible}
            onClose={onCleanClose}
            aria-labelledby="ScrollingLongContentExampleLabel"
        >
            <CModalHeader>
                <CModalTitle id="ScrollingLongContentExampleLabel">Edit order</CModalTitle>
            </CModalHeader>
            <CModalBody>
                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Customer</label>
                    </div>
                    <div className="col-sm-9">
                        <div>Full name: {addData.fullName}</div>
                        <div>Phone: {addData.phone}</div>
                        <div>Email: {addData.email}</div>
                        <div>Address: {addData.address}</div>
                        <div>City: {addData.city}</div>
                        <div>Country: {addData.country}</div>
                    </div>
                </div>

                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Order</label>
                    </div>
                    <div className="col-sm-9">
                        <div>
                            <div>Product:</div>
                            {
                                addData.products.map((e, i) => (
                                    <div style={{ marginLeft: "1rem" }}>
                                        {i + 1}/ {e.productName}: ${e.productPrice} x {e.quantity} = ${e.productPrice * e.quantity}
                                    </div>
                                ))
                            }
                        </div>
                        <div>Cost: ${addData.cost}</div>
                        <div>Note: {addData.note}</div>
                        <div>Order date: {addData.orderDate}</div>
                    </div>
                </div>

                <div className="row form-group">
                    <div className="col-sm-3">
                        <label>Shipped Date</label>
                    </div>
                    <div className="col-sm-9">
                        <input
                            onKeyDown={(e) => e.preventDefault()}
                            min={addData.orderDate ? getMinShippesDate(addData.orderDate) : null}
                            type="date"
                            className="form-control"
                            onChange={(event) => {
                                //console.log();
                                setAddData({ ...addData, shippedDate: event.target.value ? convertDay(event.target.value) : null });
                            }}
                            value={addData.shippedDate ? getMinShippesDate(addData.shippedDate) : null}
                        />
                    </div>
                </div>
            </CModalBody>
            <CModalFooter>
                {
                    errors.length == 0 ? <></> : <CAlert color="danger" className="d-flex w-100">
                        <CIcon icon={cilBurn} className="flex-shrink-0 me-2" width={24} height={24} />
                        <div>
                            <div style={{ fontWeight: "600" }}>Không thể sửa order</div>
                            {
                                errors.map((e) => {
                                    return (
                                        <div>{e}</div>
                                    );
                                })
                            }
                        </div>
                    </CAlert>
                }

                {
                    (messageApi == "Update thông tin order thành công") ? <>
                        <CAlert color="success" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Sửa order thành công!
                        </CAlert>
                    </> : <></>
                }

                {
                    (messageApi == "Unauthorized!") ? <>
                        <CAlert color="danger" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Tài khoản chưa được cấp quyền thực thi!
                        </CAlert>
                    </> : <></>
                }

                {
                    (messageApi != "" && messageApi != "Update thông tin order thành công" && messageApi != "Unauthorized!") ? <>
                        <CAlert color="danger" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Đã có lỗi!
                        </CAlert>
                    </> : <></>
                }

                <div>
                    <CButton color="secondary" onClick={onCleanClose}>
                        Close
                    </CButton>
                    <CButton onClick={onEditBtnClicked} color="success" style={{ color: "white", marginLeft: "8px" }}>Edit order</CButton>
                </div>
            </CModalFooter>
        </CModal>
    );
}

export default EditOrderModal;