/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { CAlert, CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { useEffect, useState } from "react";
import { useStore2Dispatch, useStore2Selector } from "src/store";
import { convertDay } from "./Order";
import { clearMessageApiOrder, clearOrderByID, editOrder, fetchOrder } from "src/actions/order.action";

const DeleteOrderModal = ({ visible, onClose }) => {
    const dispatch = useStore2Dispatch();
    const { messageApi, currentPage, limit, filter, orderById } = useStore2Selector(reduxData => reduxData.orderReducer);

    const [addData, setAddData] = useState({
        fullName: "",
        phone: "",
        email: "",
        address: "",
        city: "",
        country: "",

        products: [],
        cost: 0,
        note: "",
        orderDate: "",
        shippedDate: "",
    });

    const onCleanClose = () => {
        setAddData({
            fullName: "",
            phone: "",
            email: "",
            address: "",
            city: "",
            country: "",

            products: [],
            cost: 0,
            note: "",
            orderDate: "",
            shippedDate: "",
        });
        dispatch(clearMessageApiOrder());
        dispatch(clearOrderByID());
        onClose();
    }

    const onDeleteBtnClicked = () => {
        dispatch(editOrder({ isDel: true }, orderById._id));
    }

    useEffect(() => {
        if (orderById) {
            setAddData({
                fullName: orderById.customer.fullName,
                phone: orderById.customer.phone,
                email: orderById.customer.email,
                address: orderById.customer.address,
                city: orderById.customer.city,
                country: orderById.customer.country,

                products: orderById.orderDetails,
                cost: orderById.cost,
                note: orderById.note,
                orderDate: convertDay(orderById.orderDate),
                shippedDate: orderById.shippedDate ? convertDay(orderById.shippedDate) : "Chưa giao hàng",
            });
        }
    }, [orderById]);

    useEffect(() => {
        if (messageApi == "Update thông tin order thành công" || messageApi == "Không tìm thấy thông tin order") {
            dispatch(fetchOrder(currentPage, limit, filter));
        }

    }, [messageApi]);

    console.log(messageApi)
    return (
        <CModal
            visible={visible}
            onClose={onCleanClose}
            aria-labelledby="ScrollingLongContentExampleLabel"
        >
            <CModalHeader>
                <CModalTitle id="ScrollingLongContentExampleLabel">Delete order</CModalTitle>
            </CModalHeader>
            <CModalBody>
                <div className="text-danger mb-3 text-center" style={{ fontWeight: "600", fontSize: "large" }}>
                    Bạn có chắc muốn xóa đơn hàng này?
                </div>
                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Customer</label>
                    </div>
                    <div className="col-sm-9">
                        <div>Full name: {addData.fullName}</div>
                        <div>Phone: {addData.phone}</div>
                        <div>Email: {addData.email}</div>
                        <div>Address: {addData.address}</div>
                        <div>City: {addData.city}</div>
                        <div>Country: {addData.country}</div>
                    </div>
                </div>

                <div className="row form-group">
                    <div className="col-sm-3">
                        <label>Order</label>
                    </div>
                    <div className="col-sm-9">
                        <div>
                            <div>Product:</div>
                            {
                                addData.products.map((e, i) => (
                                    <div style={{ marginLeft: "1rem" }}>
                                        {i + 1}/ {e.productName}: ${e.productPrice} x {e.quantity} = ${e.productPrice * e.quantity}
                                    </div>
                                ))
                            }
                        </div>
                        <div>Cost: ${addData.cost}</div>
                        <div>Note: {addData.note}</div>
                        <div>Order date: {addData.orderDate}</div>
                        <div>Shipped date: {addData.shippedDate}</div>
                    </div>
                </div>
            </CModalBody>
            <CModalFooter>
                {
                    (messageApi == "Update thông tin order thành công") ? <>
                        <CAlert color="success" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Xóa order thành công!
                        </CAlert>
                    </> : <></>
                }

                {
                    (messageApi == "Unauthorized!") ? <>
                        <CAlert color="danger" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Tài khoản chưa được cấp quyền thực thi!
                        </CAlert>
                    </> : <></>
                }

                {
                    (messageApi != "" && messageApi != "Update thông tin order thành công" && messageApi != "Unauthorized!") ? <>
                        <CAlert color="danger" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Đã có lỗi!
                        </CAlert>
                    </> : <></>
                }

                <div>
                    <CButton color="secondary" onClick={onCleanClose}>
                        Close
                    </CButton>
                    <CButton onClick={onDeleteBtnClicked} color="success" style={{ color: "white", marginLeft: "8px" }}>Delete order</CButton>
                </div>
            </CModalFooter>
        </CModal>
    );
}

export default DeleteOrderModal;