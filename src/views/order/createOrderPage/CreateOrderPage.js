/* eslint-disable react/jsx-key */
import { CButton } from "@coreui/react";
import { Alert, ThemeProvider, createTheme } from "@mui/material";
import CustomerInput from "./CustomerInput";
import ProductInput from "./ProductInput";
import { useEffect, useState } from "react";
import ProductSelected from "./ProductSelected";
import { useStore2Dispatch, useStore2Selector } from "src/store";
import { clearMessageApi, createOrder } from "src/actions/order.action";
import { useNavigate } from "react-router-dom";

/* eslint-disable react/react-in-jsx-scope */
const theme = createTheme({
    palette: {
        primary: {
            main: "#24d278",
        },
    },
});

const CreateOrderPage = () => {
    const navigate = useNavigate();
    const dispatch = useStore2Dispatch();
    const { customerWithOrderCreate } = useStore2Selector(reduxData => reduxData.orderReducer);
    const [errors, setErrors] = useState([]);

    const [subTotal, setSubTotal] = useState(0);
    const [selectedCustomer, setSelectedCustomer] = useState({
        fullName: "",
        phone: "",
        email: "",
        address: "",
        city: "",
        country: "",
    });

    const [selectedProducts, setSelectedProducts] = useState([]);
    const [note, setNote] = useState("");

    const tinhSubTotal = () => {
        var vSubTotal = 0;
        for (let bI = 0; bI < selectedProducts.length; bI++) {
            var vQuantity = parseInt(selectedProducts[bI].count);
            var vPrice = selectedProducts[bI].buyPrice * vQuantity;
            vSubTotal = vSubTotal + vPrice;
        }
        setSubTotal(vSubTotal);
    }

    const onCreateOrderClicked = () => {
        dispatch(clearMessageApi());
        setErrors([]);
        var vErrors = [];

        if (selectedCustomer.fullName == "" || subTotal == 0) {
            vErrors.push("Bạn chưa chọn đủ thông tin!");
        }

        setErrors(vErrors);

        if (vErrors.length == 0) {
            var vOrderDetail = selectedProducts.map((e) => {
                return {
                    productName: e.name,
                    productId: e.id,
                    productPrice: e.buyPrice,
                    quantity: e.count,
                }
            })

            dispatch(createOrder({
                ...selectedCustomer,
                orderDetails: vOrderDetail,
                cost: subTotal,
                note: note
            }));
        }
    }

    const onBackBtnClicked = () => {
        navigate(-1, { replace: true })
    }

    const renderMessApi = () => {
        if (customerWithOrderCreate.message == "") {
            return <></>;
        } else if (customerWithOrderCreate.message == "Unauthorized!") {
            return <Alert style={{ marginTop: "1rem" }} severity="error">
                Tài khoản chưa được cấp quyền thực thi!
            </Alert>
        } else if (customerWithOrderCreate.message == "Tạo order kèm customer thành công") {
            return <Alert style={{ marginTop: "1rem" }} severity="success">
                Bạn đã đặt hàng thành công
            </Alert>;
        } else if (customerWithOrderCreate.message == "Số lượng không đủ!") {
            return <Alert style={{ marginTop: "1rem", justifyContent: "center" }} severity="error">
                <div style={{ fontWeight: "600" }}>Sản phẩm vượt quá số lượng hiện có!</div>
                <ul style={{ marginTop: "1rem", marginBottom: "0rem" }}>
                    {
                        customerWithOrderCreate.amountCheck.map((e) => (
                            <li>
                                <div>Tên sản phẩm: {e.productName}</div>
                                <div>Giá sản phẩm: ${e.productPrice}</div>
                                <div>Số lượng đặt hàng: {e.quantity}</div>
                                <div>Số lượng hiện có: {e.store}</div>
                            </li>
                        ))
                    }
                </ul>
            </Alert>
        } else {
            return <Alert style={{ marginTop: "1rem" }} severity="error">
                Đã có lỗi!
            </Alert>;
        }
    }

    useEffect(() => {
        tinhSubTotal();
    }, [selectedProducts]);

    console.log(customerWithOrderCreate);
    return (
        <ThemeProvider theme={theme}>
            <div className="container" style={{ backgroundColor: "#fff", padding: "1rem 3rem" }}>
                <h2 className="text-center" style={{ margin: "2rem", color: "#1da35e" }}>Create Order</h2>
                <div className="row">
                    <div className="col-md-5" style={{ display: "flex", flexDirection: "column" }}>
                        <div>
                            <CustomerInput setSelectedCustomer={setSelectedCustomer} />

                            <div>Full name: {selectedCustomer.fullName}</div>
                            <div>Phone: {selectedCustomer.phone}</div>
                            <div>Email: {selectedCustomer.email}</div>
                            <div>Address: {selectedCustomer.address}</div>
                            <div>City: {selectedCustomer.city}</div>
                            <div>Country: {selectedCustomer.country}</div>
                        </div>

                        <textarea onChange={(e) => { setNote(e.target.value) }} className="mui-type" placeholder="Message"></textarea>
                    </div>
                    <div className="col-md-7">
                        <ProductInput selectedProducts={selectedProducts} setSelectedProducts={setSelectedProducts} />
                        <div style={{ textAlign: "center", padding: "0px", margin: "0px" }}>***</div>
                        <div className="cart-demo">
                            {
                                selectedProducts.map((e, i) => {
                                    return <ProductSelected e={e} i={i}
                                        selectedProducts={selectedProducts}
                                        setSelectedProducts={setSelectedProducts}
                                    />
                                })
                            }

                            <div style={{ marginTop: "1rem" }}>
                                <div style={{ display: "flex", justifyContent: "space-between", fontWeight: "600" }}>
                                    <div>Sub total</div>
                                    <div>${subTotal}</div>
                                </div>
                                <div style={{ display: "flex", justifyContent: "space-between", fontWeight: "600" }}>
                                    <div>Coupon</div>
                                    <div>$0</div>
                                </div>
                                <div style={{ display: "flex", justifyContent: "space-between", fontWeight: "600" }}>
                                    <div>Total</div>
                                    <div>${subTotal}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {
                    errors.length == 0 ? <></> : <Alert style={{ marginTop: "1rem" }} severity="warning">
                        {
                            errors.map((e) => {
                                return (
                                    <div>{e}</div>
                                );
                            })
                        }
                    </Alert>
                }

                {
                    renderMessApi()
                }

                <div style={{ display: "flex", justifyContent: "end", marginBottom: "3rem", marginTop: "1rem" }}>
                    <CButton onClick={onBackBtnClicked} color="secondary">Back</CButton>
                    <CButton onClick={onCreateOrderClicked} color="success" style={{ color: "white", marginLeft: "8px" }}>Create Order</CButton>
                </div>
            </div>
        </ThemeProvider>
    );
}

export default CreateOrderPage;