/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { Box, TextField } from '@mui/material';
import Autocomplete, { createFilterOptions } from '@mui/material/Autocomplete';
import { useEffect, useState } from 'react';
import { fetchProduct } from 'src/actions/product.action';
import { useStore2Dispatch, useStore2Selector } from 'src/store';

const ProductInput = ({ selectedProducts, setSelectedProducts }) => {
    const dispatch = useStore2Dispatch();
    const { products } = useStore2Selector(reduxData => reduxData.productReducer);

    useEffect(() => {
        dispatch(fetchProduct());
    }, []);

    console.log(products);
    return (
        <>
            <Autocomplete
                id="country-select-demo"
                sx={{ width: "100%" }}
                options={products}
                autoHighlight
                getOptionLabel={(option) => option.name}
                renderOption={(props, option) => (
                    <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
                        <img
                            loading="lazy"
                            width="50"
                            src={option.imageUrl}
                            alt=""
                        />
                        {option.name}
                    </Box>
                )}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        label="Product"
                        inputProps={{
                            ...params.inputProps,
                            autoComplete: 'new-password', // disable autocomplete and autofill
                        }}
                    />
                )}
                onChange={(event, newValue) => {
                    if (newValue) {
                        var vPlusCount = {
                            id: newValue._id,
                            name: newValue.name,
                            imageUrl: newValue.imageUrl,
                            buyPrice: newValue.buyPrice,
                            count: 1,
                        }

                        if (!(selectedProducts.some((e) => e.id == vPlusCount.id))) {
                            setSelectedProducts([...selectedProducts, vPlusCount]);
                        }
                    }
                }}
            />
        </>
    );
}

export default ProductInput;