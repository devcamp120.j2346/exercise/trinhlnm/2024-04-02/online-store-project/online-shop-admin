/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { TextField } from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import { useEffect, useState } from 'react';
import { fetchCustomer } from 'src/actions/customer.action';
import { useStore2Dispatch, useStore2Selector } from 'src/store';

const CustomerInput = ({ setSelectedCustomer }) => {
    const dispatch = useStore2Dispatch();
    const { customers } = useStore2Selector(reduxData => reduxData.customerReducer);
    
    const [value, setValue] = useState(null);

    useEffect(() => {
        dispatch(fetchCustomer());
    }, []);

    console.log(customers);
    return (
        <>
            <Autocomplete
                style={{width: "100%", marginBottom: "10px"}}
                value={value}
                onChange={(event, newValue) => {
                    if (typeof newValue === 'string') {

                    } else if (newValue && newValue.inputValue) {

                    } else {
                        setValue(newValue);
                    }

                    if(newValue) {
                        setSelectedCustomer({
                            fullName: newValue.fullName,
                            phone: newValue.phone,
                            email: newValue.email,
                            address: newValue.address,
                            city: newValue.city,
                            country: newValue.country,
                        });
                    }
                }}
                id="free-solo-dialog-demo"
                options={customers}
                getOptionLabel={(option) => {
                    // e.g. value selected with enter, right from the input
                    if (typeof option === 'string') {
                        return option;
                    }
                    if (option.inputValue) {
                        return option.inputValue;
                    }
                    return option.fullName;
                }}
                selectOnFocus
                clearOnBlur
                handleHomeEndKeys
                renderOption={(props, option) => <li {...props}>{option.fullName}</li>}
                freeSolo
                renderInput={(params) => <TextField {...params} label="Customer" />}
            />
        </>
    );
}

export default CustomerInput;