/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */

import { useEffect, useState } from "react";

/* eslint-disable react/react-in-jsx-scope */
const ProductSelected = ({ e, i, selectedProducts, setSelectedProducts }) => {
    const [count, setCount] = useState(1);

    e.count = count;

    const onPlusClicked = (index) => {
        setCount(count + 1);
    }

    const onMinusClicked = () => {
        if (count > 1) {
            setCount(count - 1);
        }
    }

    const onDeleteClicked = () => {
        selectedProducts.splice(i, 1);
        setSelectedProducts([...selectedProducts]);
        console.log(selectedProducts)
    }

    useEffect(() => {
        selectedProducts[i] = e;
        setSelectedProducts([...selectedProducts]);
        console.log(selectedProducts);
    }, [count]);

    return (
        <div>
            <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                <div style={{ display: "flex", alignItems: "center" }}>
                    <img src={e.imageUrl} style={{ width: "7rem", marginRight: "1rem" }} />
                    <div>
                        <div>Name: {e.name}</div>
                        <div>Price: ${e.buyPrice}</div>
                        <div style={{display: "flex", alignItems: "center"}}>
                            <img onClick={onMinusClicked} style={{ width: "1rem", height: "1rem", cursor: "pointer" }} src={require("../../../assets/images/minus-icon.png")} />
                            <div style={{margin: "0rem 0.3rem", color: "black"}}>{e.count}</div>
                            <img onClick={onPlusClicked} style={{ width: "1rem", height: "1rem", cursor: "pointer" }} src={require("../../../assets/images/plus-icon.png")} />
                        </div>
                    </div>
                </div>

                <div>
                    <div style={{textAlign: "end"}}>${e.buyPrice * e.count}</div>
                    <img onClick={onDeleteClicked} style={{ width: "1.5rem", cursor: "pointer" }} src={require("../../../assets/images/path15244.png")} />
                </div>
            </div>
            <div style={{ backgroundColor: "#adaaaa", height: "1px" }} />
        </div>
    );
}

export default ProductSelected;