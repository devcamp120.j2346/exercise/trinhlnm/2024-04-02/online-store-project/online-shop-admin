/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { useState } from "react";
import { useStore2Dispatch, useStore2Selector } from "src/store";
import CustomerInputFilter from "./CustomerInputFilter";
import { Dialog, DialogActions, DialogContent, DialogTitle, Tooltip } from "@mui/material";
import { styled } from '@mui/material/styles';
import { ThemeProvider, createTheme } from "@mui/material";
import ProductInputFilter from "./ProductInputFilter";
import { clearFilterOrderValues, fetchOrder, inputFilterCostMaxChangeAction, inputFilterCostMinChangeAction, inputFilterCustomerChangeAction, inputFilterODMaxChangeAction, inputFilterODMinChangeAction, inputFilterSDMaxChangeAction, inputFilterSDMinChangeAction, inputFilterSDToggleChangeAction, setFilterOrderAction } from "src/actions/order.action";
import { CAlert } from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { cilBurn } from '@coreui/icons'

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

const theme = createTheme({
    palette: {
        primary: {
            main: "#24d278",
        },
    },
});

const FilterOrderModal = ({ visible, onClose, reloadPage }) => {
    const dispatch = useStore2Dispatch();
    const { sdToggle, sdMin, sdMax, odMin, odMax, costMin, costMax, currentPage, limit, filter } = useStore2Selector(reduxData => reduxData.orderReducer);

    const [errors, setErrors] = useState([]);

    const onCleanClose = () => {
        dispatch(clearFilterOrderValues());
        setErrors([]);
        dispatch(fetchOrder(currentPage, limit, filter));
        onClose();
    }

    const onFilterBtnClicked = () => {
        setErrors([]);

        var vErrors = [];

        if (sdToggle && sdMin && sdMax) {
            if (new Date(sdMin).getTime() >= new Date(sdMax).getTime()) {
                vErrors.push("Ngày min cần nhỏ hơn ngày max!");
            }
        }

        if (odMin && odMax) {
            if (new Date(odMin).getTime() >= new Date(odMax).getTime()) {
                vErrors.push("Ngày min cần nhỏ hơn ngày max!");
            }
        }

        if (costMin && costMax) {
            if (costMin >= costMax) {
                vErrors.push("Cost min cần nhỏ hơn cost max!");
            }
        }

        setErrors(vErrors);

        if (vErrors.length == 0) {
            dispatch(setFilterOrderAction());
            reloadPage();
        }
    }

    const onClearFilterBtnClicked = () => {
        dispatch(clearFilterOrderValues());
        dispatch(fetchOrder(currentPage, limit, filter));
    }

    return (
        <ThemeProvider theme={theme}>
            <BootstrapDialog
                onClose={onCleanClose}
                aria-labelledby="customized-dialog-title"
                open={visible}
                fullWidth={true}
                PaperProps={{
                    style: {
                        maxHeight: '90%',
                    }
                }}
            >
                <DialogTitle sx={{ m: 0, p: 2 }} id="customized-dialog-title">
                    Order filter
                </DialogTitle>

                <DialogContent dividers>
                    <div>
                        <CustomerInputFilter />

                        <ProductInputFilter />
                    </div>

                    <div style={{ marginTop: "2rem", paddingLeft: "2rem", paddingRight: "2rem" }}>
                        <div style={{ color: sdToggle ? "#000000de" : "grey" }}>
                            <div className="row form-group mb-3">
                                <div className="col-sm-4" style={{ display: "flex", justifyContent: "space-between" }}>
                                    <Tooltip title={sdToggle ? "Click to lock this field" : "Click to unlock this field"}><label onClick={() => { dispatch(inputFilterSDToggleChangeAction(!sdToggle)) }} style={{ cursor: "pointer", textDecoration: "underline" }}>Shipped Date:</label></Tooltip>
                                    <label>From</label>
                                </div>
                                <div className="col-sm-8">
                                    <input
                                        disabled={!sdToggle}
                                        onKeyDown={(e) => e.preventDefault()}
                                        type="date"
                                        className="form-control"
                                        onChange={(event) => {
                                            dispatch(inputFilterSDMinChangeAction(event.target.value));
                                        }}
                                        value={sdMin}
                                    />
                                </div>
                            </div>
                            <div className="row form-group mb-3">
                                <div className="col-sm-4" style={{ display: "flex", justifyContent: "end" }}>
                                    <label>To</label>
                                </div>
                                <div className="col-sm-8">
                                    <input
                                        disabled={!sdToggle}
                                        onKeyDown={(e) => e.preventDefault()}
                                        type="date"
                                        className="form-control"
                                        onChange={(event) => {
                                            dispatch(inputFilterSDMaxChangeAction(event.target.value));
                                        }}
                                        value={sdMax}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="row form-group mb-3">
                            <div className="col-sm-4" style={{ display: "flex", justifyContent: "space-between" }}>
                                <label>Order Date:</label>
                                <label>From</label>
                            </div>
                            <div className="col-sm-8">
                                <input
                                    onKeyDown={(e) => e.preventDefault()}
                                    type="date"
                                    className="form-control"
                                    onChange={(event) => {
                                        dispatch(inputFilterODMinChangeAction(event.target.value));
                                    }}
                                    value={odMin}
                                />
                            </div>
                        </div>
                        <div className="row form-group mb-3">
                            <div className="col-sm-4" style={{ display: "flex", justifyContent: "end" }}>
                                <label>To</label>
                            </div>
                            <div className="col-sm-8">
                                <input
                                    onKeyDown={(e) => e.preventDefault()}
                                    type="date"
                                    className="form-control"
                                    onChange={(event) => {
                                        dispatch(inputFilterODMaxChangeAction(event.target.value));
                                    }}
                                    value={odMax}
                                />
                            </div>
                        </div>

                        <div className="row form-group">
                            <div className="col-sm-2">
                                <label>Cost</label>
                            </div>
                            <div className="col-sm-10">
                                <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                                    <div style={{ width: "45%" }}>
                                        <input
                                            type="number"
                                            placeholder="Min price"
                                            className="form-control"
                                            onChange={(event) => {
                                                dispatch(inputFilterCostMinChangeAction(event.target.value));
                                            }}
                                            value={costMin}
                                        />
                                    </div>
                                    <div>&#11166;</div>
                                    <div style={{ width: "45%" }}>
                                        <input
                                            type="number"
                                            placeholder="Max price"
                                            className="form-control"
                                            onChange={(event) => {
                                                dispatch(inputFilterCostMaxChangeAction(event.target.value));
                                            }}
                                            value={costMax}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </DialogContent>
                <DialogActions style={{ flexDirection: "column" }}>
                    {
                        errors.length == 0 ? <></> : <CAlert color="danger" className="d-flex w-100" style={{ marginBottom: "8px" }}>
                            <CIcon icon={cilBurn} className="flex-shrink-0 me-2" width={24} height={24} />
                            <div>
                                <div style={{ fontWeight: "600" }}>Không thể lọc order</div>
                                {
                                    errors.map((e) => {
                                        return (
                                            <div>{e}</div>
                                        );
                                    })
                                }
                            </div>
                        </CAlert>
                    }

                    <div style={{ width: "100%", display: "flex", justifyContent: "end" }}>
                        <button onClick={onClearFilterBtnClicked} className="btn btn-secondary">
                            Clear
                        </button>
                        <button style={{ margin: "0px 4px 0px 8px" }} onClick={onFilterBtnClicked} className="btn btn-success text-white">
                            Filter
                        </button>
                    </div>
                </DialogActions>
            </BootstrapDialog>
        </ThemeProvider>
    );
}

export default FilterOrderModal;