/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { Box, TextField } from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import { useEffect, useState } from 'react';
import { fetchCustomer } from 'src/actions/customer.action';
import { inputFilterCustomerChangeAction } from 'src/actions/order.action';
import { useStore2Dispatch, useStore2Selector } from 'src/store';

const CustomerInputFilter = () => {
    const dispatch = useStore2Dispatch();
    const { customers } = useStore2Selector(reduxData => reduxData.customerReducer);
    const { customer } = useStore2Selector(reduxData => reduxData.orderReducer);

    useEffect(() => {
        dispatch(fetchCustomer());
    }, []);

    console.log(customers);
    return (
        <>
            <Autocomplete
                id="country-select-demo"
                sx={{ width: "100%", marginBottom: "1rem" }}
                options={customers}
                autoHighlight
                getOptionLabel={(option) => option.fullName}
                renderOption={(props, option) => (
                    <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
                        {option.fullName}
                    </Box>
                )}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        label="Customer"
                        inputProps={{
                            ...params.inputProps,
                            autoComplete: 'new-password', // disable autocomplete and autofill
                        }}
                    />
                )}
                onChange={(event, newValue) => {
                    dispatch(inputFilterCustomerChangeAction(newValue))
                }}
                value={customer}
            />
        </>
    );
}

export default CustomerInputFilter;