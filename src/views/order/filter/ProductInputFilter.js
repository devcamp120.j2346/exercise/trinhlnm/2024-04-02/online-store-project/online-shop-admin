/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { Box, TextField } from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import { useEffect } from 'react';
import { inputFilterProductChangeAction } from 'src/actions/order.action';
import { fetchProduct } from 'src/actions/product.action';
import { useStore2Dispatch, useStore2Selector } from 'src/store';

const ProductInputFilter = () => {
    const dispatch = useStore2Dispatch();
    const { products } = useStore2Selector(reduxData => reduxData.productReducer);
    const { product } = useStore2Selector(reduxData => reduxData.orderReducer);

    useEffect(() => {
        dispatch(fetchProduct());
    }, []);

    return (
        <>
            <Autocomplete
                id="country-select-demo"
                sx={{ width: "100%" }}
                options={products}
                autoHighlight
                getOptionLabel={(option) => option.name}
                renderOption={(props, option) => (
                    <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
                        <img
                            loading="lazy"
                            width="50"
                            src={option.imageUrl}
                            alt=""
                        />
                        {option.name}
                    </Box>
                )}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        label="Product"
                        inputProps={{
                            ...params.inputProps,
                            autoComplete: 'new-password', // disable autocomplete and autofill
                        }}
                    />
                )}
                onChange={(event, newValue) => {
                    dispatch(inputFilterProductChangeAction(newValue));
                }}
                value={product}
            />
        </>
    );
}

export default ProductInputFilter;