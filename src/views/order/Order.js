/* eslint-disable react/jsx-key */
/* eslint-disable react/react-in-jsx-scope */

import { CAlert } from "@coreui/react";
import { Pagination, Tooltip } from "@mui/material";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { clearFilterOrderAction, clearMessageApi, fetchOrder, fetchOrderById, pageChangePaginationOrder } from "src/actions/order.action";
import { useStore2Dispatch, useStore2Selector } from "src/store";
import EditOrderModal from "./EditOrderModal";
import DeleteOrderModal from "./DeleteOrderModal";
import { cilLibraryAdd, cilFilterSquare } from '@coreui/icons';
import CIcon from "@coreui/icons-react";
import FilterOrderModal from "./filter/FilterOrderModal";

//hàm đổi ngày
export const convertDay = (day) => {
  var d = new Date(day);
  return (d.toLocaleDateString('en-GB'));
}

const Order = () => {
  const navigate = useNavigate();
  const dispatch = useStore2Dispatch();
  const { noPage, currentPage, limit, totalOrders, orders, pending, error, filter } = useStore2Selector(reduxData => reduxData.orderReducer);

  const [visibleF, setVisibleF] = useState(false);
  const [visibleE, setVisibleE] = useState(false);
  const [visibleD, setVisibleD] = useState(false);
  const [clickedIndex, setClickedIndex] = useState(-1);

  useEffect(() => {
    dispatch(fetchOrder(currentPage, limit, filter));
  }, [currentPage]);

  const onChangePagination = (event, value) => {
    dispatch(pageChangePaginationOrder(value));
  }

  const onBtnAddClicked = () => {
    dispatch(clearMessageApi());
    navigate("/create-order");
  }

  const onBtnFilterClicked = () => {
    setVisibleF(true);
  }

  const onFModalClose = () => {
    setVisibleF(false);
  }

  const onEModalClose = () => {
    setClickedIndex(-1);
    setVisibleE(false);
  }

  const onDModalClose = () => {
    setVisibleD(false);
  }

  const onEditBtnClicked = (i) => {
    setClickedIndex(i)
    setVisibleE(!visibleE);
  }

  const onDeleteBtnClicked = (id) => {
    dispatch(fetchOrderById(id));
    setVisibleD(!visibleD);
  }

  const onFilterTooltip = () => {
    dispatch(clearFilterOrderAction());
    window.location.reload();
  }

  console.log(orders);
  return (
    <>
      <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "1rem" }}>
        <div>
          <h3 style={{ display: "inline", color: "#1da35e", fontWeight: "700" }}>Order</h3>
          <Tooltip title="Click to clear filter">
            <span onClick={onFilterTooltip} style={{ fontStyle: "italic", textDecoration: "underline", cursor: "pointer", fontSize: "0.9rem", marginLeft: "8px" }}>{Object.keys(filter).every((k) => !filter[k]) ? "" : "(Filter on)"}</span>
          </Tooltip>
        </div>

        <div style={{ display: "flex" }}>
          <Tooltip title="Filter list">
            <CIcon onClick={onBtnFilterClicked} style={{ width: "1.5rem", color: "#1da35e", marginRight: "0.5rem", cursor: "pointer" }} icon={cilFilterSquare} customClassName="nav-icon" />
          </Tooltip>
          <Tooltip title="Add new item">
            <CIcon onClick={onBtnAddClicked} style={{ width: "1.6rem", color: "#1da35e", cursor: "pointer" }} icon={cilLibraryAdd} customClassName="nav-icon" />
          </Tooltip>
        </div>
      </div>

      {
        pending ? <div>Loading...</div> : <>
          {
            orders.length != 0 ? <>
              <table className="table table-striped table-hover">
                <thead>
                  <tr>
                    <th scope="col"></th>
                    <th scope="col">Customer</th>
                    <th scope="col">Cost</th>
                    <th scope="col">Product</th>
                    <th scope="col">Order date</th>
                    <th scope="col">Shipped date</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    pending ? <></> : orders.map((e, i) => {
                      return (
                        <tr>
                          <td style={{ fontWeight: "bold" }}>{i + (currentPage - 1) * limit + 1}</td>
                          <td>{e.customer.fullName}</td>
                          <td>${e.cost}</td>
                          <td>{
                            e.orderDetails.map((detail) => (<div>
                              {detail.productName}: ${detail.productPrice} x {detail.quantity}
                            </div>))
                          }</td>
                          <td>{convertDay(e.orderDate)}</td>
                          <td>{e.shippedDate ? convertDay(e.shippedDate) : "Chưa giao hàng"}</td>
                          <td>
                            <button onClick={() => onEditBtnClicked(i)} style={{ color: "white", marginRight: "10px" }} className='btn btn-info'>Edit</button>
                            <button onClick={() => onDeleteBtnClicked(e._id)} style={{ color: "white" }} className='btn btn-danger'>Delete</button>
                          </td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>

              <div style={{ display: "flex", justifyContent: "space-between", margin: "1rem 0rem" }}>
                <div>Total: {totalOrders} orders</div>
                <Pagination count={noPage} shape="rounded"
                  onChange={onChangePagination}
                  page={currentPage}
                />
              </div>
            </> : <>No orders</>
          }
        </>
      }

      {
        error ? <CAlert color="danger" variant="solid" style={{ textAlign: "center", fontSize: "larger", fontWeight: "600" }}>
          Đã có lỗi!
        </CAlert> : <></>
      }

      <FilterOrderModal visible={visibleF} onClose={onFModalClose} reloadPage={() => { window.location.reload(); }} />
      <EditOrderModal visible={visibleE} onClose={onEModalClose} info={orders[clickedIndex]} />
      <DeleteOrderModal visible={visibleD} onClose={onDModalClose} />
    </>
  );
}
export default Order
