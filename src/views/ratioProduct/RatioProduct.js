import { CCard, CCardBody, CCardHeader, CCol, CRow } from "@coreui/react";
import { CChartBar, CChartPie } from "@coreui/react-chartjs";

/* eslint-disable react/react-in-jsx-scope */
const RatioProduct = () => {
  return (
    <CRow>
      <CCol xs={6}>
        <CCard className="mb-4">
          <CCardHeader>Sản phẩm mua nhiều nhất</CCardHeader>
          <CCardBody>
            <CChartPie
              data={{
                labels: ['Name 1', 'Name 2', 'Khác'],
                datasets: [
                  {
                    data: [300, 50, 100],
                    backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
                    hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
                  },
                ],
              }}
            />
          </CCardBody>
        </CCard>
      </CCol>

      <CCol xs={6}>
        <CCard className="mb-4">
          <CCardHeader>Thống kê theo thời gian</CCardHeader>
          <CCardBody>
            <CChartBar
              data={{
                labels: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7'],
                datasets: [
                  {
                    label: 'Name 1',
                    backgroundColor: '#FF6384',
                    data: [40, 20, 12, 39, 10, 40, 39, 80, 40],
                  },
                ],
              }}
              labels="months"
            />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
}
export default RatioProduct;
