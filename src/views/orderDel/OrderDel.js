/* eslint-disable react/jsx-key */
/* eslint-disable react/react-in-jsx-scope */

import { CAlert } from "@coreui/react";
import { Pagination } from "@mui/material";
import { useEffect, useState } from "react";
import { editOrder, fetchOrder, fetchOrderById, fetchOrderDel, pageChangePaginationOrderDel } from "src/actions/order.action";
import { useStore2Dispatch, useStore2Selector } from "src/store";
import DelOrderModal from "./DelOrderModal";

//hàm đổi ngày
export const convertDay = (day) => {
  var d = new Date(day);
  return (d.toLocaleDateString('en-GB'));
}

const OrderDel = () => {
  const dispatch = useStore2Dispatch();
  const { messageApi, noPageDel, currentPageDel, limit, totalOrderDel, orderDels, pending, error } = useStore2Selector(reduxData => reduxData.orderReducer);

  const [visibleD, setVisibleD] = useState(false);

  useEffect(() => {
    dispatch(fetchOrderDel(currentPageDel, limit));
  }, [currentPageDel]);

  useEffect(() => {
    if (messageApi == "Update thông tin order thành công") {
      window.location.reload();
    }
  }, [messageApi]);

  const onChangePagination = (event, value) => {
    dispatch(pageChangePaginationOrderDel(value));
  }

  const onDModalClose = () => {
    setVisibleD(false);
  }

  const onRestoreBtnClicked = (id) => {
    dispatch(editOrder({ isDel: false }, id));
  }

  const onDeleteBtnClicked = (id) => {
    dispatch(fetchOrderById(id));
    setVisibleD(!visibleD);
  }

  console.log(orderDels);
  return (
    <>
      <div style={{ display: "flex", justifyContent: "start", marginBottom: "1rem" }}>
        <h3 style={{ color: "#1da35e", fontWeight: "600" }}>Deleted Order</h3>
      </div>

      {
        pending ? <div>Loading...</div> : <>
          {
            orderDels.length != 0 ? <>
              <table className="table table-striped table-hover">
                <thead>
                  <tr>
                    <th scope="col"></th>
                    <th scope="col">Customer</th>
                    <th scope="col">Cost</th>
                    <th scope="col">Product</th>
                    <th scope="col">Order date</th>
                    <th scope="col">Shipped date</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    pending ? <></> : orderDels.map((e, i) => {
                      return (
                        <tr>
                          <td style={{ fontWeight: "bold" }}>{i + (currentPageDel - 1) * limit + 1}</td>
                          <td>{e.customer.fullName}</td>
                          <td>${e.cost}</td>
                          <td>{
                            e.orderDetails.map((detail) => (<div>
                              {detail.productName}: ${detail.productPrice} x {detail.quantity}
                            </div>))
                          }</td>
                          <td>{convertDay(e.orderDate)}</td>
                          <td>{e.shippedDate ? convertDay(e.shippedDate) : "Chưa giao hàng"}</td>
                          <td>
                            <button onClick={() => onRestoreBtnClicked(e._id)} style={{ color: "white", marginRight: "10px" }} className='btn btn-info'>Restore</button>
                            <button onClick={() => onDeleteBtnClicked(e._id)} style={{ color: "white" }} className='btn btn-danger'>Delete</button>
                          </td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>

              <div style={{ display: "flex", justifyContent: "space-between", margin: "1rem 0rem" }}>
                <div>Total: {totalOrderDel} orders</div>
                <Pagination count={noPageDel} shape="rounded"
                  onChange={onChangePagination}
                  page={currentPageDel}
                />
              </div>
            </> : <>No deleted orders</>
          }
        </>
      }

      {
        error ? <CAlert color="danger" variant="solid" style={{ textAlign: "center", fontSize: "larger", fontWeight: "600" }}>
          Đã có lỗi!
        </CAlert> : <></>
      }

      <DelOrderModal visible={visibleD} onClose={onDModalClose} />
    </>
  );
}
export default OrderDel
