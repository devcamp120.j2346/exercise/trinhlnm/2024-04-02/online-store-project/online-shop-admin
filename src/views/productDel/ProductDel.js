/* eslint-disable react/jsx-key */

import { Pagination } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { editProduct, fetchProductById, fetchProductDel, pageChangePaginationDel } from 'src/actions/product.action'
import { useStore2Dispatch, useStore2Selector } from 'src/store'
import { CAlert } from '@coreui/react'
import DelModal from './DelModal'

const ProductDel = () => {
  const [visibleD, setVisibleD] = useState(false);

  const dispatch = useStore2Dispatch();
  const { messageApi, noPageDel, currentPageDel, limit, totalProductDel, productDels, productById, pending, error } = useStore2Selector(reduxData => reduxData.productReducer);

  const onChangePagination = (event, value) => {
    dispatch(pageChangePaginationDel(value));
  }

  const onRestoreBtnClicked = (id) => {
    const formData = new FormData();
    formData.append("isDel", "false");
    dispatch(editProduct(formData, id));
  }

  const onDeleteBtnClicked = (id) => {
    dispatch(fetchProductById(id));
    setVisibleD(!visibleD);
  }

  const onDModalClose = () => {
    setVisibleD(false);
  }

  useEffect(() => {
    dispatch(fetchProductDel(currentPageDel, limit));
  }, [currentPageDel]);

  useEffect(() => {
    if (messageApi == "Update thông tin product thành công") {
      window.location.reload();
    }
}, [messageApi]);

  console.log(productDels);
  return (
    <>
      <div style={{ display: "flex", justifyContent: "start", marginBottom: "1rem" }}>
        <h3 style={{ color: "#1da35e", fontWeight: "600" }}>Deleted Product</h3>
      </div>

      {
        pending ? <>Loading...</> : <>
          {
            productDels.length != 0 ? <>
              <table className="table table-striped table-hover">
                <thead>
                  <tr>
                    <th scope="col"></th>
                    <th scope="col">Name</th>
                    <th scope="col">Type</th>
                    <th scope="col">Image</th>
                    <th scope="col">Buy price</th>
                    <th scope="col">Promotion<br />price</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    pending ? <></> : productDels.map((e, i) => {
                      return (
                        <tr>
                          <td style={{ fontWeight: "bold" }}>{i + (currentPageDel - 1) * limit + 1}</td>
                          <td>{e.name}</td>
                          <td>{e.type.name}</td>
                          <td>
                            <img src={e.imageUrl} className='img-thumbnail' style={{ width: "8rem" }} />
                          </td>
                          <td>{e.buyPrice} $</td>
                          <td>{e.promotionPrice} $</td>
                          <td>
                            <button onClick={() => onRestoreBtnClicked(e._id)} style={{ color: "white", marginRight: "10px" }} className='btn btn-info'>Restore</button>
                            <button onClick={() => onDeleteBtnClicked(e._id)} style={{ color: "white" }} className='btn btn-danger'>Delete</button>
                          </td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>

              <div style={{ display: "flex", justifyContent: "space-between", margin: "1rem 0rem" }}>
                <div>Total: {totalProductDel} products</div>
                <Pagination count={noPageDel} shape="rounded"
                  onChange={onChangePagination}
                  page={currentPageDel}
                />
              </div>
            </> : <div>No deleted products</div>
          }
        </>
      }

      {
        error ? <CAlert color="danger" variant="solid" style={{ textAlign: "center", fontSize: "larger", fontWeight: "600" }}>
          Đã có lỗi!
        </CAlert> : <></>
      }

      <DelModal visible={visibleD} onClose={onDModalClose} info={productById} />
    </>
  )
}

export default ProductDel
