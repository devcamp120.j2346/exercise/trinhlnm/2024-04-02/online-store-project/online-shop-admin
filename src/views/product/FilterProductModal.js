/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import CIcon from "@coreui/icons-react";
import { cilBurn } from '@coreui/icons'
import { CAlert, CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { useEffect, useState } from "react";
import { useStore2Dispatch, useStore2Selector } from "src/store";
import { clearFilterValues, inputFilterMaxPriceChangeAction, inputFilterMinPriceChangeAction, inputFilterNameChangeAction, inputFilterProductTypeChangeAction, setFilterAction, setFilterValues } from "src/actions/product.action";

const FilterProductModal = ({ visible, onClose, reloadPage }) => {
    const dispatch = useStore2Dispatch();
    const { productTypes, name, minPrice, maxPrice, productType } = useStore2Selector(reduxData => reduxData.productReducer);

    const [errors, setErrors] = useState([]);

    const onCleanClose = () => {
        dispatch(clearFilterValues());
        setErrors([]);
        onClose();
    }

    const onFilterBtnClicked = () => {
        setErrors([]);

        var vErrors = [];

        if (minPrice && minPrice < 0) {
            vErrors.push("Min price cần > 0!");
        }

        if (maxPrice && maxPrice < 0) {
            vErrors.push("Max price cần > 0!");
        }

        if (minPrice && maxPrice && Number(minPrice) > Number(maxPrice)) {
            vErrors.push("Min price không thể lớn hơn max price!");
        }

        setErrors(vErrors);

        if (vErrors.length == 0) {
            dispatch(setFilterAction());
            reloadPage();
        }
    }

    const onClearFilterBtnClicked = () => {
        dispatch(inputFilterNameChangeAction(""));
        dispatch(inputFilterMinPriceChangeAction(""));
        dispatch(inputFilterMaxPriceChangeAction(""));
        dispatch(inputFilterProductTypeChangeAction(""));
    }

    useEffect(() => {
        dispatch(setFilterValues());
    }, [])

    return (
        <CModal
            visible={visible}
            onClose={onCleanClose}
            aria-labelledby="ScrollingLongContentExampleLabel"
        >
            <CModalHeader>
                <CModalTitle id="ScrollingLongContentExampleLabel">Filter product</CModalTitle>
            </CModalHeader>
            <CModalBody style={{ paddingLeft: "3rem", paddingRight: "3rem" }}>
                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Name</label>
                    </div>
                    <div className="col-sm-9">
                        <input
                            type="text"
                            placeholder="Product name"
                            className="form-control"
                            onChange={(event) => {
                                dispatch(inputFilterNameChangeAction(event.target.value));
                            }}
                            value={name}
                        />
                    </div>
                </div>

                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Price</label>
                    </div>
                    <div className="col-sm-9">
                        <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                            <div style={{ width: "45%" }}>
                                <input
                                    type="number"
                                    placeholder="Min price"
                                    className="form-control"
                                    onChange={(event) => {
                                        dispatch(inputFilterMinPriceChangeAction(event.target.value));
                                    }}
                                    value={minPrice}
                                />
                            </div>
                            <div>&#11166;</div>
                            <div style={{ width: "45%" }}>
                                <input
                                    type="number"
                                    placeholder="Max price"
                                    className="form-control"
                                    onChange={(event) => {
                                        dispatch(inputFilterMaxPriceChangeAction(event.target.value));
                                    }}
                                    value={maxPrice}
                                />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row form-group">
                    <div className="col-sm-3">
                        <label>Type</label>
                    </div>
                    <div className="col-sm-9">
                        <select
                            className="form-control"
                            onChange={(event) => {
                                dispatch(inputFilterProductTypeChangeAction(event.target.value));
                            }}
                            value={productType}
                        >
                            <option value="">Select product type</option>
                            {
                                productTypes.length == 0 ? <></> : <>
                                    {
                                        productTypes.map((e) => {
                                            return (
                                                <option value={e._id}>{e.name}</option>
                                            );
                                        })
                                    }
                                </>
                            }
                        </select>
                    </div>
                </div>
            </CModalBody>
            <CModalFooter>
                {
                    errors.length == 0 ? <></> : <CAlert color="danger" className="d-flex w-100">
                        <CIcon icon={cilBurn} className="flex-shrink-0 me-2" width={24} height={24} />
                        <div>
                            <div style={{ fontWeight: "600" }}>Không thể lọc sản phẩm</div>
                            {
                                errors.map((e) => {
                                    return (
                                        <div>{e}</div>
                                    );
                                })
                            }
                        </div>
                    </CAlert>
                }

                <div>
                    <CButton color="secondary" onClick={onClearFilterBtnClicked}>
                        Clear
                    </CButton>
                    <CButton onClick={onFilterBtnClicked} color="success" style={{ color: "white", marginLeft: "8px" }}>Filter</CButton>
                </div>
            </CModalFooter>
        </CModal>
    );
}

export default FilterProductModal;