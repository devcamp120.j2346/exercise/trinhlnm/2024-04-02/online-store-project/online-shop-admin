/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import CIcon from "@coreui/icons-react";
import { cilBurn } from '@coreui/icons'
import { CAlert, CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { useEffect, useState } from "react";
import { useStore2Dispatch, useStore2Selector } from "src/store";
import { clearMessageApi, createProduct } from "src/actions/product.action";
import { cilCloudUpload } from '@coreui/icons';

const AddProductModal = ({ visible, onClose, reloadPage }) => {
    const dispatch = useStore2Dispatch();
    const { productTypes, messageApi } = useStore2Selector(reduxData => reduxData.productReducer);

    const [selectedFile, setSelectedFile] = useState(null);
    const [selectedName, setSelectedName] = useState("");
    const [addData, setAddData] = useState({
        name: "",
        description: "",
        type: "",
        image: "",
        buyPrice: 0,
        promotionPrice: 0,
        amount: 0
    });
    const [errors, setErrors] = useState([]);

    const handleChange = (e) => {
        setAddData({ ...addData, [e.target.name]: e.target.value });
    }

    const handleFileChange = (event) => {
        const file = event.target.files[0];
        setSelectedFile(URL.createObjectURL(file));
        setSelectedName(file.name);
        setAddData({ ...addData, image: file });
    };

    const onCleanClose = () => {
        setSelectedFile(null);
        setSelectedName("");
        setAddData({
            name: "",
            description: "",
            type: "",
            image: "",
            buyPrice: 0,
            promotionPrice: 0,
            amount: 0
        });
        setErrors([]);
        dispatch(clearMessageApi());
        onClose();
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("name", addData.name);
        formData.append("description", addData.description);
        formData.append("type", addData.type);
        formData.append("image", addData.image);
        formData.append("buyPrice", addData.buyPrice);
        formData.append("promotionPrice", addData.promotionPrice);
        formData.append("amount", addData.amount);

        // for (var key of formData.entries()) {
        //     console.log(key[0] + ', ' + key[1]);
        // }
        onAddBtnClicked(formData);
    }

    const onAddBtnClicked = (formData) => {
        dispatch(clearMessageApi());
        setErrors([]);

        var vErrors = [];

        if (addData.name == "" || addData.description == "" || addData.type == "" || addData.image == "" || addData.buyPrice == 0 || addData.promotionPrice == 0 || addData.amount == 0) {
            vErrors.push("Bạn chưa điền đủ thông tin!");
        }

        if (addData.buyPrice < 0 || addData.promotionPrice < 0 || addData.amount < 0) {
            vErrors.push("Price hoặc amount cần > 0!");
        }

        if (Number(addData.promotionPrice) > Number(addData.buyPrice)) {
            vErrors.push("Promotion price không thể lớn hơn buy price!");
        }

        if (Number.isInteger(Number(addData.amount))) {

        } else {
            vErrors.push("Amount cần là số nguyên!");
        }

        setErrors(vErrors);

        if (vErrors.length == 0) {
            dispatch(createProduct(formData));
        }
    }

    useEffect(() => {
        if (messageApi == "Tạo product thành công") {
            reloadPage();
        }
    }, [messageApi]);

    console.log(productTypes);
    console.log(messageApi);
    return (
        <CModal
            visible={visible}
            onClose={onCleanClose}
            aria-labelledby="ScrollingLongContentExampleLabel"
        >
            <CModalHeader>
                <CModalTitle id="ScrollingLongContentExampleLabel">Add product</CModalTitle>
            </CModalHeader>
            <CModalBody>
                <form onSubmit={handleSubmit} encType='multipart/form-data'>
                    <div className="row form-group mb-2">
                        <div className="col-sm-3">
                            <label>Name</label>
                        </div>
                        <div className="col-sm-9">
                            <input
                                name="name"
                                type="text"
                                placeholder="Product name"
                                className="form-control"
                                onChange={handleChange}
                                value={addData.name}
                            />
                        </div>
                    </div>

                    <div className="row form-group mb-2">
                        <div className="col-sm-3">
                            <label>Description</label>
                        </div>
                        <div className="col-sm-9">
                            <input
                                name="description"
                                type="text"
                                placeholder="Product description"
                                className="form-control"
                                onChange={handleChange}
                                value={addData.description}
                            />
                        </div>
                    </div>

                    <div className="row form-group mb-2">
                        <div className="col-sm-3">
                            <label>Type</label>
                        </div>
                        <div className="col-sm-9">
                            <select
                                name="type"
                                className="form-control"
                                onChange={handleChange}
                                value={addData.type}
                            >
                                <option value="">Select product type</option>
                                {
                                    productTypes.length == 0 ? <></> : <>
                                        {
                                            productTypes.map((e) => {
                                                return (
                                                    <option value={e._id}>{e.name}</option>
                                                );
                                            })
                                        }
                                    </>
                                }
                            </select>
                        </div>
                    </div>

                    <div className="row form-group mb-2">
                        <div className="col-sm-3">
                            <label>Buy Price ($)</label>
                        </div>
                        <div className="col-sm-9">
                            <input
                                name="buyPrice"
                                type="number"
                                placeholder="Product buy price"
                                className="form-control"
                                onChange={handleChange}
                                value={addData.buyPrice}
                            />
                        </div>
                    </div>

                    <div className="row form-group mb-2">
                        <div className="col-sm-3">
                            <label>Promotion Price ($)</label>
                        </div>
                        <div className="col-sm-9 d-flex align-items-center">
                            <input
                                name="promotionPrice"
                                type="number"
                                placeholder="Product promotion price"
                                className="form-control"
                                onChange={handleChange}
                                value={addData.promotionPrice}
                            />
                        </div>
                    </div>

                    <div className="row form-group mb-2">
                        <div className="col-sm-3">
                            <label>Amount</label>
                        </div>
                        <div className="col-sm-9">
                            <input
                                name="amount"
                                type="number"
                                placeholder="Amount of product"
                                className="form-control"
                                onChange={handleChange}
                                value={addData.amount}
                            />
                        </div>
                    </div>

                    <div className="row form-group" style={{ justifyContent: "center", marginTop: "1rem" }}>
                        <div className="file-upload">
                            <div style={{ width: "100%", minHeight: "5rem", display: "flex", justifyContent: "center", alignItems: "center" }}>
                                {
                                    selectedFile ? <img style={{ width: "50%" }} src={selectedFile} /> : <CIcon style={{ width: "3rem" }} icon={cilCloudUpload} customClassName="nav-icon" />
                                }
                            </div>
                            <p style={{ marginBottom: "0rem" }}>{selectedName || "Click box to upload"}</p>
                            <input name="image" type="file" accept=".png, .jpg, .jpeg" onChange={handleFileChange} />
                        </div>
                    </div>
                </form>
            </CModalBody>
            <CModalFooter>
                {
                    errors.length == 0 ? <></> : <CAlert color="danger" className="d-flex w-100">
                        <CIcon icon={cilBurn} className="flex-shrink-0 me-2" width={24} height={24} />
                        <div>
                            <div style={{ fontWeight: "600" }}>Không thể thêm sản phẩm</div>
                            {
                                errors.map((e) => {
                                    return (
                                        <div>{e}</div>
                                    );
                                })
                            }
                        </div>
                    </CAlert>
                }

                {
                    (messageApi == "Tạo product thành công") ? <>
                        <CAlert color="success" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Tạo product thành công!
                        </CAlert>
                    </> : <></>
                }

                {
                    (messageApi == "Unauthorized!") ? <>
                        <CAlert color="danger" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Tài khoản chưa được cấp quyền thực thi!
                        </CAlert>
                    </> : <></>
                }

                {
                    (messageApi != "" && messageApi != "Tạo product thành công" && messageApi != "Unauthorized!") ? <>
                        <CAlert color="danger" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Đã có lỗi!
                        </CAlert>
                    </> : <></>
                }

                <div>
                    <CButton color="secondary" onClick={onCleanClose}>
                        Close
                    </CButton>
                    <CButton onClick={handleSubmit} color="success" style={{ color: "white", marginLeft: "8px" }}>Add product</CButton>
                </div>
            </CModalFooter>
        </CModal>
    );
}

export default AddProductModal;