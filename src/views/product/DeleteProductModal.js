/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import { CAlert, CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { useStore2Dispatch, useStore2Selector } from "src/store";
import { clearMessageApi, clearProductByID, editProduct, fetchProduct } from "src/actions/product.action";
import { useEffect, useState } from "react";

const DeleteProductModal = ({ visible, onClose, info }) => {
    const dispatch = useStore2Dispatch();
    const { messageApi, currentPage, limit, filter } = useStore2Selector(reduxData => reduxData.productReducer);

    const [productData, setProductData] = useState({
        name: "",
        description: "",
        type: "",
        imageUrl: "",
        buyPrice: 0,
        promotionPrice: 0,
        amount: 0
    });

    const onCleanClose = () => {
        setProductData({
            name: "",
            description: "",
            type: "",
            imageUrl: "",
            buyPrice: 0,
            promotionPrice: 0,
            amount: 0
        });
        console.log(messageApi)
        dispatch(clearMessageApi());
        dispatch(clearProductByID());
        onClose();
    }

    const onDeleteBtnClicked = () => {
        const formData = new FormData();
        formData.append("isDel", "true");
        dispatch(editProduct(formData, info._id));
    }

    const onImageUrlImgError = (e) => {
        e.target.src = 'https://imgs.search.brave.com/huRuHXgwn2aCRJYgLa07SwPnEaMY8WykKxRr9MhYSPE/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9jZG4u/dmVjdG9yc3RvY2su/Y29tL2kvcHJldmll/dy0xeC8zOS8yNi9p/c29sYXRlZC1wYXct/Zm9vdC1vbi1ncmVl/bi1iYWNrZ3JvdW5k/LXZlY3Rvci0yNzA3/MzkyNi5qcGc';
    }

    useEffect(() => {
        if (info) {
            setProductData({
                name: info.name,
                description: info.description,
                type: info.type.name,
                imageUrl: info.imageUrl,
                buyPrice: info.buyPrice,
                promotionPrice: info.promotionPrice,
                amount: info.amount
            });
        }
    }, [info]);

    useEffect(() => {
        if (messageApi == "Update thông tin product thành công") {
            dispatch(fetchProduct(currentPage, limit, filter.name, filter.minPrice, filter.maxPrice, filter.productType));
        }
    }, [messageApi]);

    console.log(messageApi);
    return (
        <CModal
            visible={visible}
            onClose={onCleanClose}
            aria-labelledby="ScrollingLongContentExampleLabel"
        >
            <CModalHeader>
                <CModalTitle id="ScrollingLongContentExampleLabel">Delete product</CModalTitle>
            </CModalHeader>
            <CModalBody>
                <div className="text-danger mb-3 text-center" style={{ fontWeight: "600", fontSize: "large" }}>
                    Bạn có chắc muốn xóa sản phẩm này?
                </div>

                <div className="row" style={{ padding: "0rem 2rem" }}>
                    <div className="col-md-6" style={{ display: "flex", alignItems: "center" }}>
                        <img onError={onImageUrlImgError} style={{ width: "100%" }} src={productData.imageUrl} alt="" />
                    </div>
                    <div className="col-md-6" style={{ display: "flex", flexDirection: "column", justifyContent: "center" }}>
                        <div>Name: {productData.name}</div>
                        <div>Description: {productData.description}</div>
                        <div>Type: {productData.type}</div>
                        <div>Buy Price: ${productData.buyPrice}</div>
                        <div>Promotion Price: ${productData.promotionPrice}</div>
                        <div>Amount: {productData.amount}</div>
                    </div>
                </div>
            </CModalBody>
            <CModalFooter>
                {
                    (messageApi == "Update thông tin product thành công") ? <>
                        <CAlert color="success" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Xóa product thành công!
                        </CAlert>
                    </> : <></>
                }

                {
                    (messageApi == "Unauthorized!") ? <>
                        <CAlert color="danger" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Tài khoản chưa được cấp quyền thực thi!
                        </CAlert>
                    </> : <></>
                }

                {
                    (messageApi != "" && messageApi != "Update thông tin product thành công" && messageApi != "Unauthorized!") ? <>
                        <CAlert color="danger" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Đã có lỗi!
                        </CAlert>
                    </> : <></>
                }

                <div>
                    <CButton color="secondary" onClick={onCleanClose}>
                        Close
                    </CButton>
                    <CButton onClick={onDeleteBtnClicked} color="success" style={{ color: "white", marginLeft: "8px" }}>Delete product</CButton>
                </div>
            </CModalFooter>
        </CModal>
    );
}

export default DeleteProductModal;