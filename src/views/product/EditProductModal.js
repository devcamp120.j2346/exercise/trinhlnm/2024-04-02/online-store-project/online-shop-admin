/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */
/* eslint-disable react/react-in-jsx-scope */
import CIcon from "@coreui/icons-react";
import { cilBurn } from '@coreui/icons'
import { CAlert, CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { useEffect, useState } from "react";
import { useStore2Dispatch, useStore2Selector } from "src/store";
import { cilCloudUpload } from '@coreui/icons';
import { clearMessageApi, clearProductByID, editProduct, fetchProduct } from "src/actions/product.action";

const EditProductModal = ({ visible, onClose, info }) => {
    console.log(info);
    const dispatch = useStore2Dispatch();
    const { productTypes, messageApi, currentPage, limit, filter } = useStore2Selector(reduxData => reduxData.productReducer);

    const [selectedFile, setSelectedFile] = useState(null);
    const [selectedName, setSelectedName] = useState("");
    const [addData, setAddData] = useState({
        name: "",
        description: "",
        type: "",
        image: "",
        buyPrice: 0,
        promotionPrice: 0,
        amount: 0
    });

    const [errors, setErrors] = useState([]);

    useEffect(() => {
        setAddData({
            name: info.name,
            description: info.description,
            type: info.type._id,
            imageUrl: info.imageUrl,
            buyPrice: info.buyPrice,
            promotionPrice: info.promotionPrice,
            amount: info.amount
        });
    }, [info]);

    useEffect(() => {
        if (messageApi == "Update thông tin product thành công") {
            dispatch(fetchProduct(currentPage, limit, filter.name, filter.minPrice, filter.maxPrice, filter.productType));
        }
    }, [messageApi]);

    const handleChange = (e) => {
        setAddData({ ...addData, [e.target.name]: e.target.value });
    }

    const handleFileChange = (event) => {
        const file = event.target.files[0];
        setSelectedFile(URL.createObjectURL(file));
        setSelectedName(file.name);
        setAddData({ ...addData, image: file });
    };

    const onImageUrlImgError = (e) => {
        e.target.src = 'https://imgs.search.brave.com/huRuHXgwn2aCRJYgLa07SwPnEaMY8WykKxRr9MhYSPE/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9jZG4u/dmVjdG9yc3RvY2su/Y29tL2kvcHJldmll/dy0xeC8zOS8yNi9p/c29sYXRlZC1wYXct/Zm9vdC1vbi1ncmVl/bi1iYWNrZ3JvdW5k/LXZlY3Rvci0yNzA3/MzkyNi5qcGc';
    }

    const onCleanClose = () => {
        setSelectedFile(null);
        setSelectedName("");
        setAddData({
            name: "",
            description: "",
            type: "",
            image: "",
            buyPrice: 0,
            promotionPrice: 0,
            amount: 0
        });
        setErrors([]);
        dispatch(clearProductByID());
        dispatch(clearMessageApi());
        onClose();
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("name", addData.name);
        formData.append("description", addData.description);
        formData.append("type", addData.type);
        formData.append("image", addData.image);
        formData.append("buyPrice", addData.buyPrice);
        formData.append("promotionPrice", addData.promotionPrice);
        formData.append("amount", addData.amount);

        onEditBtnClicked(formData);
    }

    const onEditBtnClicked = (formData) => {
        setErrors([]);
        var vErrors = [];

        if (addData.name == "" || addData.description == "" || addData.type == "" || addData.imageUrl == "" || addData.buyPrice == 0 || addData.promotionPrice == 0 || addData.amount == 0) {
            vErrors.push("Bạn chưa điền đủ thông tin!");
        }

        if (addData.buyPrice < 0 || addData.promotionPrice < 0 || addData.amount < 0) {
            vErrors.push("Price hoặc amount cần > 0!");
        }

        if (Number(addData.promotionPrice) > Number(addData.buyPrice)) {
            vErrors.push("Promotion price không thể lớn hơn buy price!");
        }

        if (Number.isInteger(Number(addData.amount))) {

        } else {
            vErrors.push("Amount cần là số nguyên!");
        }

        setErrors(vErrors);

        if (vErrors.length == 0) {
            dispatch(editProduct(formData, info._id));
        }
    }

    console.log(messageApi);
    return (
        <CModal
            visible={visible}
            onClose={onCleanClose}
            aria-labelledby="ScrollingLongContentExampleLabel"
        >
            <CModalHeader>
                <CModalTitle id="ScrollingLongContentExampleLabel">Edit product</CModalTitle>
            </CModalHeader>
            <CModalBody>
                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Name</label>
                    </div>
                    <div className="col-sm-9">
                        <input
                            type="text"
                            placeholder="Product name"
                            className="form-control"
                            onChange={handleChange}
                            value={addData.name}
                        />
                    </div>
                </div>

                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Description</label>
                    </div>
                    <div className="col-sm-9">
                        <input
                            type="text"
                            name="description"
                            placeholder="Product description"
                            className="form-control"
                            onChange={handleChange}
                            value={addData.description}
                        />
                    </div>
                </div>

                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Type</label>
                    </div>
                    <div className="col-sm-9">
                        <select
                            name="type"
                            className="form-control"
                            onChange={handleChange}
                            value={addData.type}
                        >
                            <option value="">Select product type</option>
                            {
                                productTypes.length == 0 ? <></> : <>
                                    {
                                        productTypes.map((e) => {
                                            return (
                                                <option value={e._id}>{e.name}</option>
                                            );
                                        })
                                    }
                                </>
                            }
                        </select>
                    </div>
                </div>

                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Buy Price ($)</label>
                    </div>
                    <div className="col-sm-9">
                        <input
                            type="number"
                            name="buyPrice"
                            placeholder="Product buy price"
                            className="form-control"
                            onChange={handleChange}
                            value={addData.buyPrice}
                        />
                    </div>
                </div>

                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Promotion Price ($)</label>
                    </div>
                    <div className="col-sm-9 d-flex align-items-center">
                        <input
                            type="number"
                            name="promotionPrice"
                            placeholder="Product promotion price"
                            className="form-control"
                            onChange={handleChange}
                            value={addData.promotionPrice}
                        />
                    </div>
                </div>

                <div className="row form-group mb-2">
                    <div className="col-sm-3">
                        <label>Amount</label>
                    </div>
                    <div className="col-sm-9">
                        <input
                            type="number"
                            name="amount"
                            placeholder="Amount of product"
                            className="form-control"
                            onChange={handleChange}
                            value={addData.amount}
                        />
                    </div>
                </div>

                <div className="row form-group mt-3">
                    <div className="col-sm-3">
                        <label>Image</label>
                        <img style={{ width: "100%", marginTop: "8px" }} src={addData.imageUrl} alt=""
                            onError={onImageUrlImgError}
                        />
                    </div>
                    <div className="col-sm-9">
                        <div className="file-upload" style={{ width: "100%"}}>
                            <div style={{ width: "100%", minHeight: "5rem", display: "flex", justifyContent: "center", alignItems: "center" }}>
                                {
                                    selectedFile ? <img style={{ width: "50%" }} src={selectedFile} /> : <CIcon style={{ width: "3rem" }} icon={cilCloudUpload} customClassName="nav-icon" />
                                }
                            </div>
                            <p style={{ marginBottom: "0rem" }}>{selectedName || "Click box to upload"}</p>
                            <input name="image" type="file" accept=".png, .jpg, .jpeg" onChange={handleFileChange} />
                        </div>
                    </div>
                </div>
            </CModalBody>
            <CModalFooter>
                {
                    errors.length == 0 ? <></> : <CAlert color="danger" className="d-flex w-100">
                        <CIcon icon={cilBurn} className="flex-shrink-0 me-2" width={24} height={24} />
                        <div>
                            <div style={{ fontWeight: "600" }}>Không thể sửa sản phẩm</div>
                            {
                                errors.map((e) => {
                                    return (
                                        <div>{e}</div>
                                    );
                                })
                            }
                        </div>
                    </CAlert>
                }

                {
                    (messageApi == "Update thông tin product thành công") ? <>
                        <CAlert color="success" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Sửa product thành công!
                        </CAlert>
                    </> : <></>
                }

                {
                    (messageApi == "Unauthorized!") ? <>
                        <CAlert color="danger" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Tài khoản chưa được cấp quyền thực thi!
                        </CAlert>
                    </> : <></>
                }

                {
                    (messageApi != "" && messageApi != "Update thông tin product thành công" && messageApi != "Unauthorized!") ? <>
                        <CAlert color="danger" variant="solid" style={{ display: "flex", justifyContent: "center", width: "100%", fontSize: "larger", fontWeight: "600" }}>
                            Đã có lỗi!
                        </CAlert>
                    </> : <></>
                }

                <div>
                    <CButton color="secondary" onClick={onCleanClose}>
                        Close
                    </CButton>
                    <CButton onClick={handleSubmit} color="success" style={{ color: "white", marginLeft: "8px" }}>Edit product</CButton>
                </div>
            </CModalFooter>
        </CModal>
    );
}

export default EditProductModal;