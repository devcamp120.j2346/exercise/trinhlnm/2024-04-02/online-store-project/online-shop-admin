/* eslint-disable react/jsx-key */

import { Pagination, Tooltip } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { clearFilterAction, fetchProduct, fetchProductById, fetchProductType, pageChangePagination } from 'src/actions/product.action'
import { useStore2Dispatch, useStore2Selector } from 'src/store'
import AddProductModal from './AddProductModal'
import { CAlert } from '@coreui/react'
import EditProductModal from './EditProductModal'
import DeleteProductModal from './DeleteProductModal'
import CIcon from '@coreui/icons-react'
import { cilLibraryAdd, cilFilterSquare } from '@coreui/icons';
import FilterProductModal from './FilterProductModal'

const Product = () => {
  const [visibleA, setVisibleA] = useState(false);
  const [visibleE, setVisibleE] = useState(false);
  const [visibleD, setVisibleD] = useState(false);
  const [visibleF, setVisibleF] = useState(false);

  const dispatch = useStore2Dispatch();
  const { noPage, currentPage, limit, totalProduct, products, productById, pending, error, filter } = useStore2Selector(reduxData => reduxData.productReducer);

  const onChangePagination = (event, value) => {
    dispatch(pageChangePagination(value));
  }

  const onAddProductBtnClicked = () => {
    setVisibleA(!visibleA);
  }

  const onFilterClicked = () => {
    setVisibleF(!visibleF);
  }

  const onEditBtnClicked = (id) => {
    dispatch(fetchProductById(id));
    setVisibleE(!visibleE);
  }

  const onDeleteBtnClicked = (id) => {
    dispatch(fetchProductById(id));
    setVisibleD(!visibleD);
  }

  const onAModalClose = () => {
    setVisibleA(false);
  }

  const onFModalClose = () => {
    setVisibleF(false);
  }

  const onEModalClose = () => {
    setVisibleE(false);
  }

  const onDModalClose = () => {
    setVisibleD(false);
  }

  const onFilterTooltip = () => {
    dispatch(clearFilterAction());
    reloadPage();
  }

  const reloadPage = () => {
    window.location.reload();
  }

  useEffect(() => {
    dispatch(fetchProduct(currentPage, limit, filter.name, filter.minPrice, filter.maxPrice, filter.productType));
    dispatch(fetchProductType());
  }, [currentPage]);

  console.log(products);
  return (
    <>
      <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "1rem" }}>
        <div>
          <h3 style={{ display: "inline", color: "#1da35e", fontWeight: "700" }}>Product</h3>
          <Tooltip title="Click to clear filter">
            <span onClick={onFilterTooltip} style={{ fontStyle: "italic", textDecoration: "underline", cursor: "pointer", fontSize: "0.9rem", marginLeft: "8px" }}>{Object.keys(filter).every((k) => !filter[k]) ? "" : "(Filter on)"}</span>
          </Tooltip>
        </div>
        <div style={{ display: "flex" }}>
          <Tooltip title="Filter list">
            <CIcon onClick={onFilterClicked} style={{ width: "1.5rem", color: "#1da35e", marginRight: "0.5rem", cursor: "pointer" }} icon={cilFilterSquare} customClassName="nav-icon" />
          </Tooltip>
          <Tooltip title="Add new item">
            <CIcon onClick={onAddProductBtnClicked} style={{ width: "1.6rem", color: "#1da35e", cursor: "pointer" }} icon={cilLibraryAdd} customClassName="nav-icon" />
          </Tooltip>
        </div>
      </div>

      {
        pending ? <>Loading...</> : <>
          {
            products.length != 0 ? <>
              <table className="table table-striped table-hover">
                <thead>
                  <tr>
                    <th scope="col"></th>
                    <th scope="col">Name</th>
                    <th scope="col">Type</th>
                    <th scope="col">Image</th>
                    <th scope="col">Buy price</th>
                    <th scope="col">Promotion<br />price</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    pending ? <></> : products.map((e, i) => {
                      return (
                        <tr>
                          <td style={{ fontWeight: "bold" }}>{i + (currentPage - 1) * limit + 1}</td>
                          <td>{e.name}</td>
                          <td>{e.type.name}</td>
                          <td>
                            <img src={e.imageUrl} className='img-thumbnail' style={{ width: "8rem" }} />
                          </td>
                          <td>{e.buyPrice} $</td>
                          <td>{e.promotionPrice} $</td>
                          <td>
                            <button onClick={() => onEditBtnClicked(e._id)} style={{ color: "white", marginRight: "10px" }} className='btn btn-info'>Edit</button>
                            <button onClick={() => onDeleteBtnClicked(e._id)} style={{ color: "white" }} className='btn btn-danger'>Delete</button>
                          </td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>

              <div style={{ display: "flex", justifyContent: "space-between", margin: "1rem 0rem" }}>
                <div>Total: {totalProduct} products</div>
                <Pagination count={noPage} shape="rounded"
                  onChange={onChangePagination}
                  page={currentPage}
                />
              </div>
            </> : <div>No products</div>
          }
        </>
      }

      {
        error ? <CAlert color="danger" variant="solid" style={{ textAlign: "center", fontSize: "larger", fontWeight: "600" }}>
          Đã có lỗi!
        </CAlert> : <></>
      }

      <AddProductModal visible={visibleA} onClose={onAModalClose} reloadPage={reloadPage} />

      <EditProductModal visible={visibleE} onClose={onEModalClose} info={productById} />

      <DeleteProductModal visible={visibleD} onClose={onDModalClose} info={productById} />

      <FilterProductModal visible={visibleF} onClose={onFModalClose} reloadPage={reloadPage} />
    </>
  )
}

export default Product
