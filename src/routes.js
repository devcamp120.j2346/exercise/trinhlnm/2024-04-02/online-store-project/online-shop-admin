import React from 'react'

const Product = React.lazy(() => import('./views/product/Product'))
const Order = React.lazy(() => import('./views/order/Order'))
const Customer = React.lazy(() => import('./views/customer/Customer'))
const Traffic = React.lazy(() => import('./views/traffic/Traffic'))
const RatioProduct = React.lazy(() => import('./views/ratioProduct/RatioProduct'))
const RatioOrder = React.lazy(() => import('./views/ratioOrder/RatioOrder'))
const RatioCustomer = React.lazy(() => import('./views/ratioCustomer/RatioCustomer'))
const DelProduct = React.lazy(() => import('./views/productDel/ProductDel'))
const DelCustomer = React.lazy(() => import('./views/customerDel/CustomerDel'))
const DelOrder = React.lazy(() => import('./views/orderDel/OrderDel'))

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: 'data/product', name: 'Product', element: Product },
  { path: 'data/order', name: 'Order', element: Order },
  { path: 'data/customer', name: 'Customer', element: Customer },
  { path: '/traffic', name: 'Traffic', element: Traffic },
  { path: 'ratio/product', name: 'Ratio Product', element: RatioProduct },
  { path: 'ratio/order', name: 'Ratio Order', element: RatioOrder },
  { path: 'ratio/customer', name: 'Ratio Customer', element: RatioCustomer },
  { path: 'del/product', name: 'Deleted Product', element: DelProduct },
  { path: 'del/customer', name: 'Deleted Customer', element: DelCustomer },
  { path: 'del/order', name: 'Deleted Order', element: DelOrder },
]

export default routes
