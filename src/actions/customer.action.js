import { CLEAR_CUSTOMER_BY_ID, CLEAR_FILTER_CUS, CLEAR_FILTER_CUS_VALUES, CLEAR_MESSAGE_API_CUSTOMER, COUNTRYCITY_FETCH_ERROR, COUNTRYCITY_FETCH_PENDING, COUNTRYCITY_FETCH_SUCCESS, CREATE_CUSTOMER_ERROR, CREATE_CUSTOMER_PENDING, CREATE_CUSTOMER_SUCCESS, CUSTOMER_BY_ID_FETCH_ERROR, CUSTOMER_BY_ID_FETCH_PENDING, CUSTOMER_BY_ID_FETCH_SUCCESS, CUSTOMER_DEL_FETCH_ERROR, CUSTOMER_DEL_FETCH_PENDING, CUSTOMER_DEL_FETCH_SUCCESS, CUSTOMER_FETCH_ERROR, CUSTOMER_FETCH_PENDING, CUSTOMER_FETCH_SUCCESS, DELETE_CUSTOMER_ERROR, DELETE_CUSTOMER_PENDING, DELETE_CUSTOMER_SUCCESS, EDIT_CUSTOMER_ERROR, EDIT_CUSTOMER_PENDING, EDIT_CUSTOMER_SUCCESS, INPUT_FILTER_CITY_CHANGE, INPUT_FILTER_COUNTRY_CHANGE, INPUT_FILTER_FULLNAME_CHANGE, PAGE_CHANGE_PAGINATION_CUSTOMER, PAGE_CHANGE_PAGINATION_CUSTOMER_DEL, SET_FILTER_CUS, SET_FILTER_CUS_VALUES } from "src/constants/customer.constant";
import { xATHeader } from "./product.action";

export const fetchCustomer = (page, limit, fullName, city, country) => {
  return async (dispatch) => {
    if (page && limit) {
      try {
        var requestOptions = {
          method: 'GET',
        }

        await dispatch({
          type: CUSTOMER_FETCH_PENDING,
        })

        //phân trang
        var params = new URLSearchParams({
          limit: limit,
          page: page
        });

        //lọc
        if (fullName) {
          params.append("fullName", fullName);
        }

        if (city) {
          params.append("city", city);
        }

        if (country) {
          params.append("country", country);
        }

        const resCustomers = await fetch('http://localhost:8080/api/v1/customers?' + params.toString(), requestOptions)

        const dataCustomers = await resCustomers.json()

        return dispatch({
          type: CUSTOMER_FETCH_SUCCESS,
          totalCustomers: dataCustomers.total,
          data: dataCustomers.data,
        })
      } catch (error) {
        return dispatch({
          type: CUSTOMER_FETCH_ERROR,
          error: error,
        })
      }
    } else {
      try {
        var requestOptions = {
          method: 'GET',
        }

        await dispatch({
          type: CUSTOMER_FETCH_PENDING,
        })

        const resCustomers = await fetch('http://localhost:8080/api/v1/customers', requestOptions)

        const dataCustomers = await resCustomers.json()

        return dispatch({
          type: CUSTOMER_FETCH_SUCCESS,
          data: dataCustomers.data,
        })
      } catch (error) {
        return dispatch({
          type: CUSTOMER_FETCH_ERROR,
          error: error,
        })
      }
    }
  }
}

export const inputFilterFullnameChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_FULLNAME_CHANGE,
    payload: inputValue
  }
}

export const inputFilterCityChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_CITY_CHANGE,
    payload: inputValue
  }
}

export const inputFilterCountryChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_COUNTRY_CHANGE,
    payload: inputValue
  }
}

export const setFilterCusAction = () => {
  return {
    type: SET_FILTER_CUS
  }
}

export const clearFilterCusAction = () => {
  return {
    type: CLEAR_FILTER_CUS
  }
}

export const clearFilterCusValues = () => {
  return {
    type: CLEAR_FILTER_CUS_VALUES
  }
}

export const setFilterCusValues = () => {
  return {
    type: SET_FILTER_CUS_VALUES
  }
}

export const pageChangePaginationCustomer = (page) => {
  return {
    type: PAGE_CHANGE_PAGINATION_CUSTOMER,
    payload: page
  };
}

export const clearMessageApiCustomer = () => {
  return {
    type: CLEAR_MESSAGE_API_CUSTOMER
  };
}

// ==================================

export const createCustomer = (customerObj) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "POST",
        body: JSON.stringify(customerObj),
        headers: {
          'Content-type': 'application/json;charset=UTF-8',
          'x-access-token': await xATHeader()
        },
      }

      await dispatch({
        type: CREATE_CUSTOMER_PENDING
      });

      const resCreateCustomer = await fetch("http://localhost:8080/api/v1/customers", requestOptions);

      const dataCreateCustomer = await resCreateCustomer.json();

      return dispatch({
        type: CREATE_CUSTOMER_SUCCESS,
        data: dataCreateCustomer
      });
    } catch (error) {
      return dispatch({
        type: CREATE_CUSTOMER_ERROR,
        error: error
      });
    }
  }
}

// ===========================

export const fetchCustomerById = (id) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "GET"
      }

      await dispatch({
        type: CUSTOMER_BY_ID_FETCH_PENDING
      });

      const resCustomerById = await fetch("http://localhost:8080/api/v1/customers" + "/" + id, requestOptions);

      const dataCustomerById = await resCustomerById.json();

      return dispatch({
        type: CUSTOMER_BY_ID_FETCH_SUCCESS,
        data: dataCustomerById.data
      });
    } catch (error) {
      return dispatch({
        type: CUSTOMER_BY_ID_FETCH_ERROR,
        error: error
      });
    }
  }
}

export const editCustomer = (customerInfo, id) => {
  return async (dispatch) => {
    try {
      var vObjectRequest = customerInfo;

      var requestOptions = {
        method: "PUT",
        body: JSON.stringify(vObjectRequest),
        headers: {
          'Content-type': 'application/json;charset=UTF-8',
          'x-access-token': await xATHeader()
        },
      }

      await dispatch({
        type: EDIT_CUSTOMER_PENDING
      });

      const resCustomerEdit = await fetch("http://localhost:8080/api/v1/customers" + "/" + id, requestOptions);

      const dataCustomerEdit = await resCustomerEdit.json();

      return dispatch({
        type: EDIT_CUSTOMER_SUCCESS,
        data: dataCustomerEdit
      });
    } catch (error) {
      return dispatch({
        type: EDIT_CUSTOMER_ERROR,
        error: error
      });
    }
  }
}

export const clearCustomerByID = () => {
  return {
    type: CLEAR_CUSTOMER_BY_ID
  };
}

// =========================================

export const deleteCustomer = (id) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "DELETE",
        headers: {
          'x-access-token': await xATHeader()
        }
      }

      await dispatch({
        type: DELETE_CUSTOMER_PENDING
      });

      const resDeleteCustomer = await fetch("http://localhost:8080/api/v1/customers" + "/" + id, requestOptions);

      const dataDeleteCustomer = await resDeleteCustomer.json();

      return dispatch({
        type: DELETE_CUSTOMER_SUCCESS,
        data: dataDeleteCustomer
      });
    } catch (error) {
      return dispatch({
        type: DELETE_CUSTOMER_ERROR,
        error: error
      });
    }
  }
}

// ====================

export const fetchCustomerDel = (page, limit) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET',
      }

      await dispatch({
        type: CUSTOMER_DEL_FETCH_PENDING,
      })

      //phân trang
      var params = new URLSearchParams({
        limit: limit,
        page: page
      });

      //lọc
      params.append("isDel", true);

      const resCustomers = await fetch('http://localhost:8080/api/v1/customers?' + params.toString(), requestOptions);

      const dataCustomers = await resCustomers.json();

      return dispatch({
        type: CUSTOMER_DEL_FETCH_SUCCESS,
        totalCustomer: dataCustomers.total,
        data: dataCustomers.data,
      })
    } catch (error) {
      return dispatch({
        type: CUSTOMER_DEL_FETCH_ERROR,
        error: error,
      })
    }
  }
}

export const pageChangePaginationCustomerDel = (page) => {
  return {
    type: PAGE_CHANGE_PAGINATION_CUSTOMER_DEL,
    payload: page
  };
}

export const fetchCountryCity = () => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET',
      }

      await dispatch({
        type: COUNTRYCITY_FETCH_PENDING,
      })

      const resCountryCity = await fetch('https://countriesnow.space/api/v0.1/countries', requestOptions)

      const dataCountryCity = await resCountryCity.json()

      return dispatch({
        type: COUNTRYCITY_FETCH_SUCCESS,
        data: dataCountryCity.data,
      })
    } catch (error) {
      return dispatch({
        type: COUNTRYCITY_FETCH_ERROR,
        error: error,
      })
    }
  }
}
