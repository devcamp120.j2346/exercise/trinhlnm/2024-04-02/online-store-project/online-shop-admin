import { CLEAR_FILTER_ORDER, CLEAR_FILTER_ORDER_VALUES, CLEAR_MESSAGE_API, CLEAR_MESSAGE_API_ORDER, CLEAR_ORDER_BY_ID, CREATE_ORDER_ERROR, CREATE_ORDER_PENDING, CREATE_ORDER_SUCCESS, DELETE_ORDER_ERROR, DELETE_ORDER_PENDING, DELETE_ORDER_SUCCESS, EDIT_ORDER_ERROR, EDIT_ORDER_PENDING, EDIT_ORDER_SUCCESS, INPUT_FILTER_COSTMAX_CHANGE, INPUT_FILTER_COSTMIN_CHANGE, INPUT_FILTER_CUSTOMER_CHANGE, INPUT_FILTER_ODMAX_CHANGE, INPUT_FILTER_ODMIN_CHANGE, INPUT_FILTER_PRODUCT_CHANGE, INPUT_FILTER_PRODUCT_TEXT_CHANGE, INPUT_FILTER_SDMAX_CHANGE, INPUT_FILTER_SDMIN_CHANGE, INPUT_FILTER_SDTOGGLE_CHANGE, ORDER_BY_ID_FETCH_ERROR, ORDER_BY_ID_FETCH_PENDING, ORDER_BY_ID_FETCH_SUCCESS, ORDER_DEL_FETCH_ERROR, ORDER_DEL_FETCH_PENDING, ORDER_DEL_FETCH_SUCCESS, ORDER_FETCH_ERROR, ORDER_FETCH_PENDING, ORDER_FETCH_SUCCESS, PAGE_CHANGE_PAGINATION_ORDER, PAGE_CHANGE_PAGINATION_ORDER_DEL, SET_FILTER_ORDER, SET_FILTER_ORDER_VALUES } from "src/constants/order.constant";
import { xATHeader } from "./product.action";

export const fetchOrder = (page, limit, filterData) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET',
      }

      await dispatch({
        type: ORDER_FETCH_PENDING,
      })

      //phân trang
      var params = new URLSearchParams({
        limit: limit,
        page: page
      });

      //lọc
      if (filterData.customer) {
        params.append("customer", filterData.customer._id);
      }

      if (filterData.product) {
        params.append("product", filterData.product._id);
      }

      if (filterData.sdToggle) {
        if (filterData.sdMin && filterData.sdMax) {
          params.append("sdMin", filterData.sdMin);
          params.append("sdMax", filterData.sdMax);
        } else if (filterData.sdMin) { //lọc theo buyPrice chữ màu đen
          params.append("sdMin", filterData.sdMin);
        } else if (filterData.sdMax) {
          params.append("sdMax", filterData.sdMax);
        } else {
          params.append("sdToggle", filterData.sdToggle);
        }
      }

      if (filterData.odMin && filterData.odMax) {
        params.append("odMin", filterData.odMin);
        params.append("odMax", filterData.odMax);
      } else if (filterData.odMin) { //lọc theo buyPrice chữ màu đen
        params.append("odMin", filterData.odMin);
      } else if (filterData.odMax) {
        params.append("odMax", filterData.odMax);
      }

      if (filterData.costMin) {
        params.append("costMin", filterData.costMin);
      }

      if (filterData.costMax) {
        params.append("costMax", filterData.costMax);
      }

      const resOrders = await fetch('http://localhost:8080/api/v1/orders?' + params.toString(), requestOptions)
      
      const dataOrders = await resOrders.json()

      return dispatch({
        type: ORDER_FETCH_SUCCESS,
        totalOrders: dataOrders.total,
        data: dataOrders.data,
      })
    } catch (error) {
      return dispatch({
        type: ORDER_FETCH_ERROR,
        error: error,
      })
    }
  }
}

export const fetchOrderById = (id) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "GET"
      }

      await dispatch({
        type: ORDER_BY_ID_FETCH_PENDING
      });

      const resOrderById = await fetch("http://localhost:8080/api/v1/orders" + "/" + id, requestOptions);

      const dataOrderById = await resOrderById.json();

      return dispatch({
        type: ORDER_BY_ID_FETCH_SUCCESS,
        data: dataOrderById.data
      });
    } catch (error) {
      return dispatch({
        type: ORDER_BY_ID_FETCH_ERROR,
        error: error
      });
    }
  }
}

export const clearOrderByID = () => {
  return {
    type: CLEAR_ORDER_BY_ID
  };
}

export const inputFilterCustomerChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_CUSTOMER_CHANGE,
    payload: inputValue
  }
}

export const inputFilterProductChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_PRODUCT_CHANGE,
    payload: inputValue
  }
}

export const inputFilterSDToggleChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_SDTOGGLE_CHANGE,
    payload: inputValue
  }
}

export const inputFilterSDMinChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_SDMIN_CHANGE,
    payload: inputValue
  }
}

export const inputFilterSDMaxChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_SDMAX_CHANGE,
    payload: inputValue
  }
}

export const inputFilterODMinChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_ODMIN_CHANGE,
    payload: inputValue
  }
}

export const inputFilterODMaxChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_ODMAX_CHANGE,
    payload: inputValue
  }
}

export const inputFilterCostMinChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_COSTMIN_CHANGE,
    payload: inputValue
  }
}

export const inputFilterCostMaxChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_COSTMAX_CHANGE,
    payload: inputValue
  }
}

export const setFilterOrderAction = () => {
  return {
    type: SET_FILTER_ORDER
  }
}

export const clearFilterOrderAction = () => {
  return {
    type: CLEAR_FILTER_ORDER
  }
}

export const clearFilterOrderValues = () => {
  return {
    type: CLEAR_FILTER_ORDER_VALUES
  }
}

export const setFilterOrderValues = () => {
  return {
    type: SET_FILTER_ORDER_VALUES
  }
}

export const pageChangePaginationOrder = (page) => {
  return {
    type: PAGE_CHANGE_PAGINATION_ORDER,
    payload: page
  };
}

// ==========================

export const createOrder = (orderObj) => {
  return async (dispatch) => {
    try {
      var vObjectRequest = orderObj;

      var requestOptions = {
        method: "POST",
        body: JSON.stringify(vObjectRequest),
        headers: {
          'Content-type': 'application/json;charset=UTF-8',
          'x-access-token': await xATHeader()
        },
      }

      await dispatch({
        type: CREATE_ORDER_PENDING
      });

      const resCreateOrder = await fetch("http://localhost:8080/api/v1/orders/order-with-customer", requestOptions);

      const dataCreateOrder = await resCreateOrder.json();

      return dispatch({
        type: CREATE_ORDER_SUCCESS,
        data: dataCreateOrder
      });
    } catch (error) {
      return dispatch({
        type: CREATE_ORDER_ERROR,
        error: error
      });
    }
  }
}

export const editOrder = (orderInfo, id) => {
  return async (dispatch) => {
    try {
      var vObjectRequest = orderInfo;

      var requestOptions = {
        method: "PUT",
        body: JSON.stringify(vObjectRequest),
        headers: {
          'Content-type': 'application/json;charset=UTF-8',
          'x-access-token': await xATHeader()
        },
      }

      await dispatch({
        type: EDIT_ORDER_PENDING
      });

      const resEditOrder = await fetch("http://localhost:8080/api/v1/orders" + "/" + id, requestOptions);

      const dataEditOrder = await resEditOrder.json();

      return dispatch({
        type: EDIT_ORDER_SUCCESS,
        data: dataEditOrder
      });
    } catch (error) {
      return dispatch({
        type: EDIT_ORDER_ERROR,
        error: error
      });
    }
  }
}

export const clearMessageApi = () => {
  return {
    type: CLEAR_MESSAGE_API
  };
}

export const clearMessageApiOrder = () => {
  return {
    type: CLEAR_MESSAGE_API_ORDER
  };
}

export const deleteOrder = (orderId) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "DELETE",
        headers: {
          'x-access-token': await xATHeader()
        }
      }

      await dispatch({
        type: DELETE_ORDER_PENDING
      });

      const resDeleteOrder = await fetch("http://localhost:8080/api/v1/orders" + "/" + orderId, requestOptions);

      const dataDeleteOrder = await resDeleteOrder.json();

      return dispatch({
        type: DELETE_ORDER_SUCCESS,
        data: dataDeleteOrder
      });
    } catch (error) {
      return dispatch({
        type: DELETE_ORDER_ERROR,
        error: error
      });
    }
  }
}

// =============================

export const fetchOrderDel = (page, limit) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET',
      }

      await dispatch({
        type: ORDER_DEL_FETCH_PENDING,
      })

      //phân trang
      var params = new URLSearchParams({
        limit: limit,
        page: page
      });

      //lọc
      params.append("isDel", true);

      const resOrders = await fetch('http://localhost:8080/api/v1/orders?' + params.toString(), requestOptions);

      const dataOrders = await resOrders.json();

      return dispatch({
        type: ORDER_DEL_FETCH_SUCCESS,
        totalOrder: dataOrders.total,
        data: dataOrders.data,
      })
    } catch (error) {
      return dispatch({
        type: ORDER_DEL_FETCH_ERROR,
        error: error,
      })
    }
  }
}

export const pageChangePaginationOrderDel = (page) => {
  return {
    type: PAGE_CHANGE_PAGINATION_ORDER_DEL,
    payload: page
  };
}
