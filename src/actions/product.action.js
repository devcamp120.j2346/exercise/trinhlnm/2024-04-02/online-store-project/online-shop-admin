import {
  CLEAR_FILTER,
  CLEAR_FILTER_VALUES,
  CLEAR_MESSAGE_API,
  CLEAR_PRODUCT_BY_ID,
  CREATE_PRODUCT_ERROR,
  CREATE_PRODUCT_PENDING,
  CREATE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_ERROR,
  DELETE_PRODUCT_PENDING,
  DELETE_PRODUCT_SUCCESS,
  EDIT_PRODUCT_ERROR,
  EDIT_PRODUCT_PENDING,
  EDIT_PRODUCT_SUCCESS,
  INPUT_FILTER_MAX_PRICE_CHANGE,
  INPUT_FILTER_MIN_PRICE_CHANGE,
  INPUT_FILTER_NAME_CHANGE,
  INPUT_FILTER_PRODUCT_TYPE_CHANGE,
  PAGE_CHANGE_PAGINATION,
  PAGE_CHANGE_PAGINATION_DEL,
  PRODUCT_BY_ID_FETCH_ERROR,
  PRODUCT_BY_ID_FETCH_PENDING,
  PRODUCT_BY_ID_FETCH_SUCCESS,
  PRODUCT_DEL_FETCH_ERROR,
  PRODUCT_DEL_FETCH_PENDING,
  PRODUCT_DEL_FETCH_SUCCESS,
  PRODUCT_FETCH_ERROR,
  PRODUCT_FETCH_PENDING,
  PRODUCT_FETCH_SUCCESS,
  PRODUCT_TYPE_FETCH_ERROR,
  PRODUCT_TYPE_FETCH_PENDING,
  PRODUCT_TYPE_FETCH_SUCCESS,
  SET_FILTER,
  SET_FILTER_VALUES,
} from 'src/constants/product.constant'

export const xATHeader = async () => {
  var vXAccessToken = "";
  const startTime = sessionStorage.getItem("startTime");
  var endingTime = new Date().getTime() - parseInt(startTime);
  if (endingTime > 7140000) { //2h-1p
    //accessToken đã hết hạn
    const refreshToken = sessionStorage.getItem("refreshToken");

    const refreshTokenRes = await fetch("http://localhost:8080/api/auth/refreshToken", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ refreshToken: refreshToken }),
    });
    const refreshTokenData = await refreshTokenRes.json();

    //lấy new access token
    vXAccessToken = refreshTokenData.accessToken;
  } else {
    const accessToken = sessionStorage.getItem("accessToken");
    vXAccessToken = accessToken;
  }
  return vXAccessToken;
}

export const fetchProduct = (page, limit, name, minPrice, maxPrice, productType) => {
  return async (dispatch) => {
    if (page && limit) {
      try {
        var requestOptions = {
          method: 'GET',
        }

        await dispatch({
          type: PRODUCT_FETCH_PENDING,
        })

        //phân trang
        var params = new URLSearchParams({
          limit: limit,
          page: page
        });

        //lọc
        if (name) {
          params.append("name", name);
        }

        if (minPrice && maxPrice) {
          params.append("minPrice", minPrice);
          params.append("maxPrice", maxPrice);
        } else if (minPrice) { //lọc theo buyPrice chữ màu đen
          params.append("minPrice", minPrice);
        } else if (maxPrice) {
          params.append("maxPrice", maxPrice);
        }

        if (productType) {
          params.append("productType", productType);
        }

        const resProducts = await fetch('http://localhost:8080/api/v1/products?' + params.toString(), requestOptions);

        const dataProducts = await resProducts.json();

        return dispatch({
          type: PRODUCT_FETCH_SUCCESS,
          totalProduct: dataProducts.total,
          data: dataProducts.data,
        })
      } catch (error) {
        return dispatch({
          type: PRODUCT_FETCH_ERROR,
          error: error,
        })
      }
    } else {
      try {
        var requestOptions = {
          method: 'GET',
        }

        await dispatch({
          type: PRODUCT_FETCH_PENDING,
        })

        const resProducts = await fetch('http://localhost:8080/api/v1/products', requestOptions)

        const dataProducts = await resProducts.json()

        return dispatch({
          type: PRODUCT_FETCH_SUCCESS,
          data: dataProducts.data,
        })
      } catch (error) {
        return dispatch({
          type: PRODUCT_FETCH_ERROR,
          error: error,
        })
      }
    }
  }
}

export const inputFilterNameChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_NAME_CHANGE,
    payload: inputValue
  }
}

export const inputFilterMaxPriceChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_MAX_PRICE_CHANGE,
    payload: inputValue
  }
}

export const inputFilterMinPriceChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_MIN_PRICE_CHANGE,
    payload: inputValue
  }
}

export const inputFilterProductTypeChangeAction = (inputValue) => {
  return {
    type: INPUT_FILTER_PRODUCT_TYPE_CHANGE,
    payload: inputValue
  }
}

export const setFilterAction = () => {
  return {
    type: SET_FILTER
  }
}

export const clearFilterAction = () => {
  return {
    type: CLEAR_FILTER
  }
}

export const clearFilterValues = () => {
  return {
    type: CLEAR_FILTER_VALUES
  }
}

export const setFilterValues = () => {
  return {
    type: SET_FILTER_VALUES
  }
}

export const pageChangePagination = (page) => {
  return {
    type: PAGE_CHANGE_PAGINATION,
    payload: page
  };
}

// ======================

export const fetchProductType = () => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET',
      }

      await dispatch({
        type: PRODUCT_TYPE_FETCH_PENDING,
      })

      const resProductTypes = await fetch('http://localhost:8080/api/v1/product-types', requestOptions)

      const dataProductTypes = await resProductTypes.json()

      return dispatch({
        type: PRODUCT_TYPE_FETCH_SUCCESS,
        data: dataProductTypes.data,
      })
    } catch (error) {
      return dispatch({
        type: PRODUCT_TYPE_FETCH_ERROR,
        error: error,
      })
    }
  }
}

// ==================================

export const createProduct = (formData) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "POST",
        body: formData,
        headers: {
          'x-access-token': await xATHeader()
        }
      }

      await dispatch({
        type: CREATE_PRODUCT_PENDING
      });

      const resCreateProduct = await fetch("http://localhost:8080/api/v1/products", requestOptions);

      const dataCreateProduct = await resCreateProduct.json();

      return dispatch({
        type: CREATE_PRODUCT_SUCCESS,
        data: dataCreateProduct
      });
    } catch (error) {
      return dispatch({
        type: CREATE_PRODUCT_ERROR,
        error: error
      });
    }
  }
}

export const clearMessageApi = () => {
  return {
    type: CLEAR_MESSAGE_API
  };
}

// ===========================

export const fetchProductById = (id) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "GET"
      }

      await dispatch({
        type: PRODUCT_BY_ID_FETCH_PENDING
      });

      const resProductById = await fetch("http://localhost:8080/api/v1/products" + "/" + id, requestOptions);

      const dataProductById = await resProductById.json();

      return dispatch({
        type: PRODUCT_BY_ID_FETCH_SUCCESS,
        data: dataProductById.data
      });
    } catch (error) {
      return dispatch({
        type: PRODUCT_BY_ID_FETCH_ERROR,
        error: error
      });
    }
  }
}

export const editProduct = (formData, id) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "PUT",
        body: formData,
        headers: {
          'x-access-token': await xATHeader(),
        },
      }

      await dispatch({
        type: EDIT_PRODUCT_PENDING
      });

      const resProductOrder = await fetch("http://localhost:8080/api/v1/products" + "/" + id, requestOptions);

      const dataProductOrder = await resProductOrder.json();
      console.log(dataProductOrder)

      return dispatch({
        type: EDIT_PRODUCT_SUCCESS,
        data: dataProductOrder
      });
    } catch (error) {
      return dispatch({
        type: EDIT_PRODUCT_ERROR,
        error: error
      });
    }
  }
}

export const clearProductByID = () => {
  return {
    type: CLEAR_PRODUCT_BY_ID
  };
}

// =========================================

export const deleteProduct = (id) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "DELETE",
        headers: {
          'x-access-token': await xATHeader(),
        }
      }

      await dispatch({
        type: DELETE_PRODUCT_PENDING
      });

      const resDeleteProduct = await fetch("http://localhost:8080/api/v1/products" + "/" + id, requestOptions);

      const dataDeleteProduct = await resDeleteProduct.json();

      return dispatch({
        type: DELETE_PRODUCT_SUCCESS,
        data: dataDeleteProduct
      });
    } catch (error) {
      return dispatch({
        type: DELETE_PRODUCT_ERROR,
        error: error
      });
    }
  }
}

// ========================

export const fetchProductDel = (page, limit) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET',
      }

      await dispatch({
        type: PRODUCT_DEL_FETCH_PENDING,
      })

      //phân trang
      var params = new URLSearchParams({
        limit: limit,
        page: page
      });

      //lọc
      params.append("isDel", true);

      const resProducts = await fetch('http://localhost:8080/api/v1/products?' + params.toString(), requestOptions);

      const dataProducts = await resProducts.json();

      return dispatch({
        type: PRODUCT_DEL_FETCH_SUCCESS,
        totalProduct: dataProducts.total,
        data: dataProducts.data,
      })
    } catch (error) {
      return dispatch({
        type: PRODUCT_DEL_FETCH_ERROR,
        error: error,
      })
    }
  }
}

export const pageChangePaginationDel = (page) => {
  return {
    type: PAGE_CHANGE_PAGINATION_DEL,
    payload: page
  };
}
