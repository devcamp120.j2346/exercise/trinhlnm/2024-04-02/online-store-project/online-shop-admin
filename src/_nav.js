import React from 'react'
import CIcon from '@coreui/icons-react'
import { cibSquarespace, cibLaunchpad, cibReadme, cibBigCartel } from '@coreui/icons';
import { CNavItem, CNavTitle } from '@coreui/react'

const _nav = [
  {
    component: CNavItem,
    icon: <CIcon icon={cibSquarespace} customClassName="nav-icon" />,
    name: 'Traffic',
    to: '/traffic',
  },
  {
    component: CNavTitle,
    name: 'Ratio',
  },
  {
    component: CNavItem,
    icon: <CIcon icon={cibLaunchpad} customClassName="nav-icon" />,
    name: 'Ratio Product',
    to: '/ratio/product',
  },
  {
    component: CNavItem,
    icon: <CIcon icon={cibLaunchpad} customClassName="nav-icon" />,
    name: 'Ratio Customer',
    to: 'ratio/customer',
  },
  {
    component: CNavItem,
    icon: <CIcon icon={cibLaunchpad} customClassName="nav-icon" />,
    name: 'Ratio Order',
    to: '/ratio/order',
  },
  {
    component: CNavTitle,
    name: 'Data',
  },
  {
    component: CNavItem,
    icon: <CIcon icon={cibReadme} customClassName="nav-icon" />,
    name: 'Product',
    to: '/data/product',
  },
  {
    component: CNavItem,
    icon: <CIcon icon={cibReadme} customClassName="nav-icon" />,
    name: 'Customer',
    to: 'data/customer',
  },
  {
    component: CNavItem,
    icon: <CIcon icon={cibReadme} customClassName="nav-icon" />,
    name: 'Order',
    to: '/data/order',
  },
  {
    component: CNavTitle,
    name: 'Recycle bin',
  },
  {
    component: CNavItem,
    icon: <CIcon icon={cibBigCartel} customClassName="nav-icon" />,
    name: 'Deleted Product',
    to: '/del/product',
  },
  {
    component: CNavItem,
    icon: <CIcon icon={cibBigCartel} customClassName="nav-icon" />,
    name: 'Deleted Customer',
    to: 'del/customer',
  },
  {
    component: CNavItem,
    icon: <CIcon icon={cibBigCartel} customClassName="nav-icon" />,
    name: 'Deleted Order',
    to: '/del/order',
  },
]

export default _nav
