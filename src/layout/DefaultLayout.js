import React from 'react'
import { AppContent, AppSidebar, AppFooter, AppHeader } from '../components/index'
import { CAlert } from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { cilBurn } from '@coreui/icons'
import { useNavigate } from 'react-router-dom';

const DefaultLayout = () => {
  const navigate = useNavigate();

  const showLayout = () => {
    var vUserId = sessionStorage.getItem("userId");
    if (vUserId) {
      return <div>
        <AppSidebar />
        <div className="wrapper d-flex flex-column min-vh-100">
          <AppHeader />
          <div className="body flex-grow-1">
            <AppContent />
          </div>
          <AppFooter />
        </div>
      </div>
    } else {
      return <div style={{minHeight: "100vh", display: "flex", justifyContent: "center", alignItems: "center"}}>
        <CAlert color="danger" style={{display: "inline-block", textAlign: "center", padding: "2rem"}}>
          <div style={{display: "flex"}}>
            <CIcon icon={cilBurn} className="flex-shrink-0 me-2" width={24} height={24} />
            <h5>Bạn chưa đăng nhập!</h5>
          </div>
          <div onClick={onDangNhapSpanClicked} className="link">Đăng nhập</div>
        </CAlert>
      </div>
    }
  }

  const onDangNhapSpanClicked = () => {
    navigate("/login");
  }
  return (
    <>{showLayout()}</>
  )
}

export default DefaultLayout
