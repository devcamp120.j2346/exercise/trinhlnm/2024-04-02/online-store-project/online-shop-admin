import { createDispatchHook, createSelectorHook } from 'react-redux'
import { legacy_createStore as createStore } from 'redux'
import { applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './reducers'
import React from 'react'

const initialState = {
  sidebarShow: true,
  theme: 'light',
}

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case 'set':
      return { ...state, ...rest }
    default:
      return state
  }
}

export const store = createStore(changeState)

export const store2 = createStore(rootReducer, applyMiddleware(thunk))
export const store2Context = React.createContext()
export const useStore2Dispatch = createDispatchHook(store2Context)
export const useStore2Selector = createSelectorHook(store2Context)
