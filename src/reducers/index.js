import { combineReducers } from "redux";
import productReducer from "./product.reducer";
import customerReducer from "./customer.reducer";
import orderReducer from "./order.reducer";

const rootReducer = combineReducers({
    productReducer,
    customerReducer,
    orderReducer
})

export default rootReducer
