import { CLEAR_FILTER_ORDER, CLEAR_FILTER_ORDER_VALUES, CLEAR_MESSAGE_API, CLEAR_MESSAGE_API_ORDER, CLEAR_ORDER_BY_ID, CREATE_ORDER_ERROR, CREATE_ORDER_PENDING, CREATE_ORDER_SUCCESS, DELETE_ORDER_ERROR, DELETE_ORDER_PENDING, DELETE_ORDER_SUCCESS, EDIT_ORDER_ERROR, EDIT_ORDER_PENDING, EDIT_ORDER_SUCCESS, INPUT_FILTER_COSTMAX_CHANGE, INPUT_FILTER_COSTMIN_CHANGE, INPUT_FILTER_CUSTOMER_CHANGE, INPUT_FILTER_ODMAX_CHANGE, INPUT_FILTER_ODMIN_CHANGE, INPUT_FILTER_PRODUCT_CHANGE, INPUT_FILTER_PRODUCT_TEXT_CHANGE, INPUT_FILTER_SDMAX_CHANGE, INPUT_FILTER_SDMIN_CHANGE, INPUT_FILTER_SDTOGGLE_CHANGE, ORDER_BY_ID_FETCH_ERROR, ORDER_BY_ID_FETCH_PENDING, ORDER_BY_ID_FETCH_SUCCESS, ORDER_DEL_FETCH_ERROR, ORDER_DEL_FETCH_PENDING, ORDER_DEL_FETCH_SUCCESS, ORDER_FETCH_ERROR, ORDER_FETCH_PENDING, ORDER_FETCH_SUCCESS, PAGE_CHANGE_PAGINATION_ORDER, PAGE_CHANGE_PAGINATION_ORDER_DEL, SET_FILTER_ORDER } from "src/constants/order.constant";

const initialState = {
    pending: false,
    error: false,
    orders: [],
    orderById: {
        customer: {},
        orderDetails: []
    },
    limit: 5,
    noPage: 0,
    currentPage: 1,
    totalOrders: 0,
    customerWithOrderCreate: {
        message: ""
    },
    messageApi: "",

    customer: null,
    product: null,
    sdToggle: false,
    sdMin: "",
    sdMax: "",
    odMin: "",
    odMax: "",
    costMin: 0,
    costMax: 0,
    filter: {
        customer: null,
        product: null,
        sdToggle: false,
        sdMin: "",
        sdMax: "",
        odMin: "",
        odMax: "",
        costMin: 0,
        costMax: 0,
    },

    orderDels: [],
    noPageDel: 0,
    currentPageDel: 1,
    totalOrderDel: 0,
}

var filterString = localStorage.getItem("orderFilter");
if (filterString) {
    const filter = JSON.parse(filterString);
    initialState.filter = filter;
    initialState.customer = filter.customer;
    initialState.product = filter.product;
    initialState.sdToggle = filter.sdToggle;
    initialState.sdMin = filter.sdMin;
    initialState.sdMax = filter.sdMax;
    initialState.odMin = filter.odMin;
    initialState.odMax = filter.odMax;
    initialState.costMin = filter.costMin;
    initialState.costMax = filter.costMax;
}

const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case ORDER_FETCH_PENDING:
            state.pending = true;
            break;
        case ORDER_FETCH_SUCCESS:
            state.pending = false;
            state.error = false;
            state.totalOrders = action.totalOrders;
            state.noPage = Math.ceil(action.totalOrders / state.limit);
            state.orders = action.data;
            break;
        case ORDER_FETCH_ERROR:
            state.error = true;
            break;
        case PAGE_CHANGE_PAGINATION_ORDER:
            state.currentPage = action.payload;
            break;

        case ORDER_BY_ID_FETCH_PENDING:
            break;
        case ORDER_BY_ID_FETCH_SUCCESS:
            state.orderById = action.data;
            break;
        case ORDER_BY_ID_FETCH_ERROR:
            break;

        case CLEAR_ORDER_BY_ID:
            state.orderById = {
                customer: {},
                orderDetails: []
            };
            break;

        case INPUT_FILTER_CUSTOMER_CHANGE:
            state.customer = action.payload;
            break;
        case INPUT_FILTER_PRODUCT_CHANGE:
            state.product = action.payload;
            break;
        case INPUT_FILTER_SDTOGGLE_CHANGE:
            state.sdToggle = action.payload;
            break;
        case INPUT_FILTER_SDMIN_CHANGE:
            state.sdMin = action.payload;
            break;
        case INPUT_FILTER_SDMAX_CHANGE:
            state.sdMax = action.payload;
            break;
        case INPUT_FILTER_ODMIN_CHANGE:
            state.odMin = action.payload;
            break;
        case INPUT_FILTER_ODMAX_CHANGE:
            state.odMax = action.payload;
            break;
        case INPUT_FILTER_COSTMIN_CHANGE:
            state.costMin = action.payload;
            break;
        case INPUT_FILTER_COSTMAX_CHANGE:
            state.costMax = action.payload;
            break;

        case SET_FILTER_ORDER:
            state.filter.customer = state.customer;
            state.filter.product = state.product;
            state.filter.sdToggle = state.sdToggle;
            state.filter.sdMin = state.sdMin;
            state.filter.sdMax = state.sdMax;
            state.filter.odMin = state.odMin;
            state.filter.odMax = state.odMax;
            state.filter.costMin = state.costMin;
            state.filter.costMax = state.costMax;
            localStorage.setItem("orderFilter", JSON.stringify(state.filter));
            break;
        case CLEAR_FILTER_ORDER:
            state.customer = null;
            state.product = null;
            state.sdToggle = false;
            state.sdMin = "";
            state.sdMax = "";
            state.odMin = "";
            state.odMax = "";
            state.costMin = 0;
            state.costMax = 0;
            state.filter = {
                customer: null,
                product: null,
                sdToggle: false,
                sdMin: "",
                sdMax: "",
                odMin: "",
                odMax: "",
                costMin: 0,
                costMax: 0,
            };
            localStorage.removeItem("orderFilter");
            break;
        case CLEAR_FILTER_ORDER_VALUES:
            state.customer = null;
            state.product = null;
            state.sdToggle = false;
            state.sdMin = "";
            state.sdMax = "";
            state.odMin = "";
            state.odMax = "";
            state.costMin = 0;
            state.costMax = 0;

        case CREATE_ORDER_PENDING:
            state.pending = true;
            break;
        case CREATE_ORDER_SUCCESS:
            state.pending = false;
            state.customerWithOrderCreate = action.data;
            break;
        case CREATE_ORDER_ERROR:
            break;

        case EDIT_ORDER_PENDING:
            break;
        case EDIT_ORDER_SUCCESS:
            state.messageApi = action.data.message;
            break;
        case EDIT_ORDER_ERROR:
            break;

        case DELETE_ORDER_PENDING:
            break;
        case DELETE_ORDER_SUCCESS:
            state.messageApi = action.data.message;
            break;
        case DELETE_ORDER_ERROR:
            break;

        case CLEAR_MESSAGE_API:
            state.customerWithOrderCreate.message = "";
            break;
        case CLEAR_MESSAGE_API_ORDER:
            state.messageApi = "";
            break;

        case ORDER_DEL_FETCH_PENDING:
            state.pending = true;
            break;
        case ORDER_DEL_FETCH_SUCCESS:
            state.pending = false;
            state.error = false;
            state.totalOrderDel = action.totalOrder;
            state.noPageDel = Math.ceil(action.totalOrder / state.limit);
            state.orderDels = action.data;
            break;
        case ORDER_DEL_FETCH_ERROR:
            state.error = true;
            break;
        case PAGE_CHANGE_PAGINATION_ORDER_DEL:
            state.currentPageDel = action.payload;
            break;
        default:
            break;
    }

    return { ...state };
}

export default orderReducer;