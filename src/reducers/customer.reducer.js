import { CLEAR_CUSTOMER_BY_ID, CLEAR_FILTER_CUS, CLEAR_FILTER_CUS_VALUES, CLEAR_MESSAGE_API_CUSTOMER, COUNTRYCITY_FETCH_ERROR, COUNTRYCITY_FETCH_PENDING, COUNTRYCITY_FETCH_SUCCESS, CREATE_CUSTOMER_ERROR, CREATE_CUSTOMER_PENDING, CREATE_CUSTOMER_SUCCESS, CUSTOMER_BY_ID_FETCH_ERROR, CUSTOMER_BY_ID_FETCH_PENDING, CUSTOMER_BY_ID_FETCH_SUCCESS, CUSTOMER_DEL_FETCH_ERROR, CUSTOMER_DEL_FETCH_PENDING, CUSTOMER_DEL_FETCH_SUCCESS, CUSTOMER_FETCH_ERROR, CUSTOMER_FETCH_PENDING, CUSTOMER_FETCH_SUCCESS, DELETE_CUSTOMER_ERROR, DELETE_CUSTOMER_PENDING, DELETE_CUSTOMER_SUCCESS, EDIT_CUSTOMER_ERROR, EDIT_CUSTOMER_PENDING, EDIT_CUSTOMER_SUCCESS, INPUT_FILTER_CITY_CHANGE, INPUT_FILTER_COUNTRY_CHANGE, INPUT_FILTER_FULLNAME_CHANGE, PAGE_CHANGE_PAGINATION_CUSTOMER, PAGE_CHANGE_PAGINATION_CUSTOMER_DEL, SET_FILTER_CUS, SET_FILTER_CUS_VALUES } from "src/constants/customer.constant";

const initialState = {
    pending: false,
    error: false,
    customers: [],
    limit: 5,
    noPage: 0,
    currentPage: 1,
    totalCustomers: 0,
    messageApi: "",

    customerById: {},

    fullName: "",
    city: "",
    country: "",
    filter: {
        fullName: "",
        city: "",
        country: "",
    },

    customerDels: [],
    noPageDel: 0,
    currentPageDel: 1,
    totalCustomerDel: 0,

    pendingCC: false,
    countrycity: [],
    errorCC: false
}

var filterString = localStorage.getItem("customerFilter");
var filterLS = {
    fullName: "",
    city: "",
    country: "",
}
if (filterString) {
    filterLS = JSON.parse(filterString);
    initialState.filter = filterLS;
    initialState.fullName = filterLS.fullName;
    initialState.city = filterLS.city;
    initialState.country = filterLS.country;
}

const customerReducer = (state = initialState, action) => {
    switch (action.type) {
        case CUSTOMER_FETCH_PENDING:
            state.pending = true;
            break;
        case CUSTOMER_FETCH_SUCCESS:
            state.pending = false;
            state.error = false;
            state.totalCustomers = action.totalCustomers;
            state.noPage = Math.ceil(action.totalCustomers / state.limit);
            state.customers = action.data;
            break;
        case CUSTOMER_FETCH_ERROR:
            state.error = true;
            break;
        case PAGE_CHANGE_PAGINATION_CUSTOMER:
            state.currentPage = action.payload;
            break;

        case INPUT_FILTER_FULLNAME_CHANGE:
            state.fullName = action.payload;
            break;
        case INPUT_FILTER_CITY_CHANGE:
            state.city = action.payload;
            break;
        case INPUT_FILTER_COUNTRY_CHANGE:
            state.country = action.payload;
            break;

        case SET_FILTER_CUS:
            state.filter.fullName = state.fullName;
            state.filter.city = state.city;
            state.filter.country = state.country;
            localStorage.setItem("customerFilter", JSON.stringify(state.filter));
            break;
        case CLEAR_FILTER_CUS:
            state.fullName = "";
            state.city = "";
            state.country = "";
            state.filter = {
                fullName: "",
                city: "",
                country: "",
            }
            localStorage.removeItem("customerFilter");
            break;
        case CLEAR_FILTER_CUS_VALUES:
            state.fullName = "";
            state.city = "";
            state.country = "";
        case SET_FILTER_CUS_VALUES:
            state.fullName = filterLS.fullName;
            state.city = filterLS.city;
            state.country = filterLS.country;

        case CREATE_CUSTOMER_PENDING:
            break;
        case CREATE_CUSTOMER_SUCCESS:
            state.messageApi = action.data.message;
            break;
        case CREATE_CUSTOMER_ERROR:
            break;
        case CLEAR_MESSAGE_API_CUSTOMER:
            state.messageApi = "";
            break;

        case CUSTOMER_BY_ID_FETCH_PENDING:
            break;
        case CUSTOMER_BY_ID_FETCH_SUCCESS:
            state.customerById = action.data;
            break;
        case CUSTOMER_BY_ID_FETCH_ERROR:
            break;

        case CLEAR_CUSTOMER_BY_ID:
            state.customerById = {};
            break;

        case EDIT_CUSTOMER_PENDING:
            break;
        case EDIT_CUSTOMER_SUCCESS:
            state.messageApi = action.data.message;
            break;
        case EDIT_CUSTOMER_ERROR:
            break;

        case DELETE_CUSTOMER_PENDING:
            break;
        case DELETE_CUSTOMER_SUCCESS:
            state.messageApi = action.data.message;
            break;
        case DELETE_CUSTOMER_ERROR:
            break;

        case CUSTOMER_DEL_FETCH_PENDING:
            state.pending = true;
            break;
        case CUSTOMER_DEL_FETCH_SUCCESS:
            state.pending = false;
            state.error = false;
            state.totalCustomerDel = action.totalCustomer;
            state.noPageDel = Math.ceil(action.totalCustomer / state.limit);
            state.customerDels = action.data;
            break;
        case CUSTOMER_DEL_FETCH_ERROR:
            state.error = true;
            break;
        case PAGE_CHANGE_PAGINATION_CUSTOMER_DEL:
            state.currentPageDel = action.payload;
            break;

        case COUNTRYCITY_FETCH_PENDING:
            state.pendingCC = true;
            break;
        case COUNTRYCITY_FETCH_SUCCESS:
            state.pendingCC = false;
            state.errorCC = false;
            state.countrycity = action.data;
            break;
        case COUNTRYCITY_FETCH_ERROR:
            state.errorCC = true;
            break;
        default:
            break;
    }

    return { ...state };
}

export default customerReducer;