import { CLEAR_FILTER, CLEAR_FILTER_VALUES, CLEAR_MESSAGE_API, CLEAR_PRODUCT_BY_ID, CREATE_PRODUCT_ERROR, CREATE_PRODUCT_PENDING, CREATE_PRODUCT_SUCCESS, DELETE_PRODUCT_ERROR, DELETE_PRODUCT_PENDING, DELETE_PRODUCT_SUCCESS, EDIT_PRODUCT_ERROR, EDIT_PRODUCT_PENDING, EDIT_PRODUCT_SUCCESS, INPUT_FILTER_MAX_PRICE_CHANGE, INPUT_FILTER_MIN_PRICE_CHANGE, INPUT_FILTER_NAME_CHANGE, INPUT_FILTER_PRODUCT_TYPE_CHANGE, PAGE_CHANGE_PAGINATION, PAGE_CHANGE_PAGINATION_DEL, PRODUCT_BY_ID_FETCH_ERROR, PRODUCT_BY_ID_FETCH_PENDING, PRODUCT_BY_ID_FETCH_SUCCESS, PRODUCT_DEL_FETCH_ERROR, PRODUCT_DEL_FETCH_PENDING, PRODUCT_DEL_FETCH_SUCCESS, PRODUCT_FETCH_ERROR, PRODUCT_FETCH_PENDING, PRODUCT_FETCH_SUCCESS, PRODUCT_FILTER_FETCH_ERROR, PRODUCT_FILTER_FETCH_PENDING, PRODUCT_FILTER_FETCH_SUCCESS, PRODUCT_TYPE_FETCH_ERROR, PRODUCT_TYPE_FETCH_PENDING, PRODUCT_TYPE_FETCH_SUCCESS, SET_FILTER, SET_FILTER_VALUES } from "src/constants/product.constant";

const initialState = {
  pending: false,
  error: false,
  products: [],
  productById: {
    imageUrl: "",
    type: {
      _id: ""
    }
  },
  pendingType: false,
  errorType: false,
  productTypes: [],
  limit: 5,
  noPage: 0,
  currentPage: 1,
  totalProduct: 0,
  messageApi: "",

  name: "",
  minPrice: 0,
  maxPrice: 0,
  productType: "",
  filter: {
    name: "",
    minPrice: 0,
    maxPrice: 0,
    productType: "",
  },

  productDels: [],
  noPageDel: 0,
  currentPageDel: 1,
  totalProductDel: 0,
}

var filterString = localStorage.getItem("productFilter");
var filterLS = {
  name: "",
  maxPrice: 0,
  minPrice: 0,
  productType: ""
};
if (filterString) {
  filterLS = JSON.parse(filterString);
  initialState.filter = filterLS;
  initialState.name = filterLS.name;
  initialState.maxPrice = filterLS.maxPrice;
  initialState.minPrice = filterLS.minPrice;
  initialState.productType = filterLS.productType;
}

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case PRODUCT_FETCH_PENDING:
      state.pending = true;
      break;
    case PRODUCT_FETCH_SUCCESS:
      state.pending = false;
      state.error = false;
      state.totalProduct = action.totalProduct;
      state.noPage = Math.ceil(action.totalProduct / state.limit);
      state.products = action.data;
      break;
    case PRODUCT_FETCH_ERROR:
      state.error = true;
      break;
    case PAGE_CHANGE_PAGINATION:
      state.currentPage = action.payload;
      break;

    case INPUT_FILTER_NAME_CHANGE:
      state.name = action.payload;
      break;
    case INPUT_FILTER_MAX_PRICE_CHANGE:
      state.maxPrice = action.payload;
      break;
    case INPUT_FILTER_MIN_PRICE_CHANGE:
      state.minPrice = action.payload;
      break;
    case INPUT_FILTER_PRODUCT_TYPE_CHANGE:
      state.productType = action.payload;
      break;

    case SET_FILTER:
      state.filter.name = state.name;
      state.filter.maxPrice = state.maxPrice;
      state.filter.minPrice = state.minPrice;
      state.filter.productType = state.productType;
      localStorage.setItem("productFilter", JSON.stringify(state.filter));
      break;
    case CLEAR_FILTER:
      state.name = "";
      state.maxPrice = 0;
      state.minPrice = 0;
      state.productType = "";
      state.filter = {
        name: "",
        minPrice: 0,
        maxPrice: 0,
        productType: "",
      };
      localStorage.removeItem("productFilter");
      break;
    case CLEAR_FILTER_VALUES:
      state.name = "";
      state.maxPrice = 0;
      state.minPrice = 0;
      state.productType = "";
    case SET_FILTER_VALUES:
      state.name = filterLS.name;
      state.maxPrice = filterLS.maxPrice;
      state.minPrice = filterLS.minPrice;
      state.productType = filterLS.productType;

    case PRODUCT_TYPE_FETCH_PENDING:
      state.pendingType = true;
      break;
    case PRODUCT_TYPE_FETCH_SUCCESS:
      state.pendingType = false;
      state.errorType = false;
      state.productTypes = action.data;
      break;
    case PRODUCT_TYPE_FETCH_ERROR:
      state.errorType = true;
      break;

    case CREATE_PRODUCT_PENDING:
      break;
    case CREATE_PRODUCT_SUCCESS:
      state.messageApi = action.data.message;
      break;
    case CREATE_PRODUCT_ERROR:
      break;
    case CLEAR_MESSAGE_API:
      state.messageApi = "";
      break;

    case PRODUCT_BY_ID_FETCH_PENDING:
      break;
    case PRODUCT_BY_ID_FETCH_SUCCESS:
      state.productById = action.data;
      break;
    case PRODUCT_BY_ID_FETCH_ERROR:
      break;

    case CLEAR_PRODUCT_BY_ID:
      state.productById = {
        imageUrl: "",
        type: {
          _id: ""
        }
      };
      break;

    case EDIT_PRODUCT_PENDING:
      break;
    case EDIT_PRODUCT_SUCCESS:
      state.messageApi = action.data.message;
      break;
    case EDIT_PRODUCT_ERROR:
      break;

    case DELETE_PRODUCT_PENDING:
      break;
    case DELETE_PRODUCT_SUCCESS:
      state.messageApi = action.data.message;
      break;
    case DELETE_PRODUCT_ERROR:
      break;

    case PRODUCT_DEL_FETCH_PENDING:
      state.pending = true;
      break;
    case PRODUCT_DEL_FETCH_SUCCESS:
      state.pending = false;
      state.error = false;
      state.totalProductDel = action.totalProduct;
      state.noPageDel = Math.ceil(action.totalProduct / state.limit);
      state.productDels = action.data;
      break;
    case PRODUCT_DEL_FETCH_ERROR:
      state.error = true;
      break;
    case PAGE_CHANGE_PAGINATION_DEL:
      state.currentPageDel = action.payload;
      break;
    default:
      break;
  }

  return { ...state };
}

export default productReducer;
